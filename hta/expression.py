__all__ = ["HTDataset", "quantile_normalize"]

import pandas as pd
import numpy as np

from hta.data.ncbi import Entrez

class HTDataset(object):
    """
    Represents a single high-throughput dataset with associated 
    feature and sample metadata.
    """
    def __init__(self, design, data, features=None, taxon_id=None):
        """
        Construct a new :class:`HTDataset`.

        Arguments
        ---------
        design: :class:`pandas.DataFrame`
            A design matrix with sample IDs as rows and factors as columns.
        data: :class:`pandas.DataFrame`
            A data matrix (e.g., expression matrix) with features (probe/gene)
            as rows and samples as columns.
        features: :class:`pandas.DataFrame`, optional
            A table containing attributes of the features (probe/genes) being
            assayed. If omitted, the features are assumed to be Entrez Gene IDs.
        taxon_id: int, optional
            The NCBI Taxonomy ID of the organism being assayed in this dataset. 
        """
        assert (design.index == data.columns).all()
        if features is not None:
            assert len(features.index) == len(data.index)
            assert (features.index == data.index).all()
        elif taxon_id is not None:
            genes = Entrez.genes(taxon_id)
            assert len(set(data.index) - set(genes.index)) == 0
            features = genes.loc[data.index,:]

        # TODO: infer taxon_id from IDs?
        self.design = design
        self.data = data
        self.features = features
        self.taxon_id = taxon_id

    def __repr__(self):
        return "<ExpressionSet with {} features and {} samples>".format(*self.data.shape)

    def normalize(self, method="quantile"):
        assert method in ("quantile",)

        if method == "quantile":
            Xn = quantile_normalize(self.data)
        features = self.features
        if features is not None:
            features = features.loc[Xn.index,:]
        return HTDataset(self.design, Xn, features=features, taxon_id=self.taxon_id)

    def contrast(self, factor, formula=None, add_feature_data=True):
        if formula is not None:
            raise NotImplementedError

        sort_on = "FDR"

        import hta.r
        formula = " + ".join(self.design.columns)
        X = self.data
        o = hta.r.limma(X, self.design,
                formula=formula, 
                coefficients=[factor])\
                        .sort(sort_on)\
                        .dropna(axis=0)
        o["SLPV"] = o["lp"] * o["t"].apply(np.sign)
        del o["lp"]
        columns = list(o.columns)
        columns[columns.index(factor)] = "logFC"
        o.columns = columns
        if (self.features is not None) and add_feature_data:
            o = self.features.join(o, how="inner")\
                    .sort(sort_on)
        return o

def quantile_normalize(X):
    """
    Quantile normalize a matrix.

    Parameters
    ----------
    X : :class:`pandas.DataFrame`
        The matrix to be normalized, with samples as columns
        and probes/genes as rows. Must not contain NaN.

    Returns
    -------
    :class:`pandas.DataFrame`
        The normalized matrix.
    """
    X = X.dropna(axis=0)
    assert isinstance(X, pd.DataFrame)
    assert X.isnull().sum().sum() == 0
    mu = X.mean(axis=1)                                             
    mu.sort()                                                       
    mu = np.array(mu)                                               
    Xn = pd.DataFrame(np.zeros(X.shape),                            
            index=X.index, columns=X.columns)                       
                                                                    
    for c in X.columns:                                             
        x = X.loc[:,c]
        ix = x.order().index                                     
        Xn.loc[ix,c] = mu                                              
    return Xn
