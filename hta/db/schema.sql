CREATE TABLE taxon (
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR,
    common_name VARCHAR
);

CREATE TABLE gene (
    id INTEGER PRIMARY KEY NOT NULL,
    taxon_id INTEGER,
    symbol VARCHAR,
    name VARCHAR,

    FOREIGN KEY (taxon_id) REFERENCES taxon (id)
);

CREATE TABLE gene_accession (
    gene_id INTEGER NOT NULL,
    nucleotide_accession VARCHAR,
    nucleotide_accession_version INTEGER,

    FOREIGN KEY (gene_id) REFERENCES gene (id)
);

-- Ontology

CREATE TABLE ontology (
    id INTEGER PRIMARY KEY NOT NULL,
    key VARCHAR,
    name VARCHAR
);

CREATE TABLE term (
    id INTEGER PRIMARY KEY NOT NULL,
    ontology_id INTEGER NOT NULL,
    key VARCHAR UNIQUE,
    name VARCHAR,
    namespace VARCHAR,

    FOREIGN KEY (ontology_id) REFERENCES ontology (id)
);

CREATE TABLE term_term (
    agent_id INTEGER NOT NULL,
    target_id INTEGER NOT NULL,
    type VARCHAR,

    FOREIGN KEY (agent_id) REFERENCES term (id),
    FOREIGN KEY (target_id) REFERENCES term (id)
);

CREATE TABLE term_closure (
    ancestor_id INTEGER NOT NULL,
    descendant_id INTEGER NOT NULL,

    FOREIGN KEY (ancestor_id) REFERENCES term(id),
    FOREIGN KEY (descendant_id) REFERENCES term(id)
);

CREATE TABLE term_synonym (
    term_id INTEGER NOT NULL,
    synonym VARCHAR,

    FOREIGN KEY (term_id) REFERENCES term (id)
);

-- gene-term mappings

CREATE TABLE gene_term (
    gene_id INTEGER NOT NULL,
    term_id INTEGER NOT NULL,
    evidence VARCHAR,

    FOREIGN KEY (term_id) REFERENCES term (id),
    FOREIGN KEY (gene_id) REFERENCES gene (id)
);

-- GEO

CREATE TABLE geo_platform (
    id INTEGER PRIMARY KEY NOT NULL,
    title VARCHAR
);

CREATE TABLE geo_series (
    id INTEGER PRIMARY KEY NOT NULL,
    title VARCHAR,
    summary VARCHAR,
    design VARCHAR
);

CREATE TABLE geo_sample (
    id INTEGER PRIMARY KEY NOT NULL,
    geo_platform_id INTEGER NOT NULL,
    title VARCHAR,
    description VARCHAR,

    FOREIGN KEY (geo_platform_id) REFERENCES geo_platform (id)
);

CREATE TABLE geo_channel (
    geo_sample_id INTEGER NOT NULL,
    channel INTEGER NOT NULL,
    source_name VARCHAR,
    molecule VARCHAR,
    characteristics VARCHAR,
    taxon_id INTEGER,

    PRIMARY KEY (geo_sample_id, channel),
    FOREIGN KEY (geo_sample_id) REFERENCES geo_sample (id)
);

CREATE TABLE geo_series_sample (
    geo_series_id INTEGER NOT NULL,
    geo_sample_id INTEGER NOT NULL,

    FOREIGN KEY (geo_series_id) REFERENCES geo_series (id),
    FOREIGN KEY (geo_sample_id) REFERENCES geo_sample (id),
    UNIQUE (geo_series_id, geo_sample_id)
);

CREATE TABLE geo_label (
    geo_sample_id INTEGER NOT NULL,
    channel INTEGER NOT NULL,

    gender INTEGER,
    age FLOAT,
    tissue_id INTEGER,

    PRIMARY KEY (geo_sample_id, channel),
    FOREIGN KEY (geo_sample_id) REFERENCES sample (id),
    FOREIGN KEY (geo_sample_id, channel) REFERENCES channel (sample_id, channel),
    FOREIGN KEY (tissue_id) REFERENCES term (id)
);
