"""
The hta.db package wraps a local SQLite database that stores various
commonly-used data from various sources, including:

- NCBI taxonomy, gene, and GEO
- Open Biomedical Ontologies

This database is built the first time it is needed, and cached locally. It is
then used transparently throughout various hta modules, such as
hta.data.ncbi.entrez.
"""

import sqlite3
import tarfile
import os
import os.path

import pandas as pd

import hta.util
from hta.util import LOG, fs, CONFIG

def taxon_records():
    scientific = {}
    common = {}
    url = "ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz"
    path = hta.util.download(url)
    tar = tarfile.open(path, "r:gz")
    handle = tar.extractfile("names.dmp")
    for i,line in enumerate(handle):
        fields = [c.strip("\t") for c in
                line.decode("utf-8")\
                        .rstrip("\n").split("|")][:-1]
        taxon_id = int(fields[0])
        name = fields[1]
        unique_name = fields[2]
        name_class = fields[3]
        if name_class == "scientific name":
            scientific[taxon_id] = name
        elif "common name" in name_class:
            common[taxon_id] = name
    for taxon_id in scientific:
        yield taxon_id, scientific[taxon_id], common.get(taxon_id)

def gene_records():
    url = "ftp://ftp.ncbi.nih.gov/gene/DATA/gene_info.gz"
    path = hta.util.download(url)
    df = pd.read_table(path,
            compression="gzip",
            skiprows=1,
            names=["Taxon ID", "Gene ID", "Symbol", "Name"],
            usecols=[0,1,2,11])
    df.drop_duplicates("Gene ID", inplace=True)
    
    for taxon_id, gene_id, symbol, name in df.to_records(index=False):
        symbol = None if symbol == "-" else symbol
        name = None if name == "-" else name
        yield int(gene_id), int(taxon_id), symbol, name

def gene_accession_records():
    url = "ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2accession.gz"
    path = hta.util.download(url)
    df = pd.read_csv(path, 
            header=None,
            sep="\t",
            compression="gzip", skiprows=1,
            usecols=[1,3],
            names=["Gene ID", "Nucleotide Accession"])
    df = df.ix[df["Nucleotide Accession"] != "-",:]
    for gene_id, n_acc in df.to_records(index=False):
        n, n_v = n_acc.split(".")
        yield int(gene_id), n, int(n_v)

def initialize_core(db):
    c = db.cursor()
    
    LOG.info("Initializing local NCBI database")
    # taxon
    LOG.debug("loading table: taxon")
    c.executemany("INSERT INTO taxon VALUES (?,?,?)",
            taxon_records())
    db.commit()
    
    # gene
    LOG.debug("loading table: gene")
    c.execute("SELECT id FROM taxon")
    taxa = set(r[0] for r in c)
    c.executemany("INSERT INTO gene VALUES (?,?,?,?)", 
                  (r for r in gene_records() if r[1] in taxa))
    db.commit()

    # gene_accession
    LOG.debug("loading table: gene_accession")
    c.execute("SELECT id FROM gene;")
    genes = set(r[0] for r in c)
    c.executemany("INSERT INTO gene_accession VALUES (?,?,?)",
            (r for r in gene_accession_records() if r[0] in genes))
    db.commit()

def initialize_ontology(db, key, name):
    LOG.info("loading ontology: {} ({})".format(key, name))
    c = db.cursor()
    o = hta.ontology.fetch(key)

    c.execute("INSERT INTO ontology (key, name) VALUES (?,?);", (key,name))
    ontology_id = c.lastrowid

    c.executemany("""INSERT INTO term (ontology_id, key, name, namespace) 
        VALUES (?,?,?,?);""", 
        ([ontology_id] + list(row) for row in o.terms.to_records()))

    c.execute("SELECT key,id FROM term WHERE ontology_id=?;", (ontology_id,))
    term_map = dict(c)

    c.executemany("""INSERT INTO term_synonym VALUES (?,?)""",
            ((term_map[key], synonym) for key,synonym in o.synonyms.to_records()))

    c.executemany("""INSERT INTO term_term (agent_id, target_id, type) VALUES (?,?,?);""",
            ((term_map[e1], term_map[e2], t) for (e1,e2,t) in 
                o.relations.to_records(index=False)
                if ((e1 in term_map) and (e2 in term_map))))

    # Include term as its own ancestor/descendant
    c.executemany("""INSERT INTO term_closure (ancestor_id, descendant_id) VALUES (?,?);""",
            [(term_map[a], term_map[d]) for a,d in o.ancestry_table.to_records(index=False)
                if ((a in term_map) and (d in term_map))]
            + [(id,id) for id in term_map.values()])

def initialize_ontologies(db):
    initialize_ontology(db, "BTO", "BRENDA Tissue Ontology")
    initialize_ontology(db, "GO", "Gene Ontology")
    #initialize_ontology(db, "DO", "Disease Ontology")

def _geometadb_connect():
    # FIXME: probably should use python gzip
    opath = fs.join(CONFIG["cache"]["root"], "geo", "GEOmetadb.sqlite")
    if not fs.exists(opath):
        os.makedirs(fs.dirname(opath), exist_ok=True)
        URL = "http://gbnci.abcc.ncifcrf.gov/geo/GEOmetadb.sqlite.gz"
        ipath = hta.util.download(URL)
        exe = fs.which("pigz") or "gzip"
        with open(opath, "wb") as h:
            sp.check_call([exe, "-cd", ipath], stdout=h)
    return sqlite3.connect(opath)

def initialize_geo(db, taxon_map):
    LOG.info("inserting GEO tables")
    mdb = _geometadb_connect()
    mc = mdb.cursor()
    c = db.cursor()

    mc.execute("SELECT CAST(SUBSTR(gpl,4) AS integer), title FROM gpl;")
    c.executemany("INSERT INTO geo_platform VALUES (?,?);", iter(mc))

    mc.execute("""SELECT CAST(SUBSTR(gse,4) AS integer), title, summary, overall_design 
        FROM gse;""")
    c.executemany("INSERT INTO geo_series VALUES (?,?,?,?);", iter(mc))

    mc.execute("""SELECT CAST(SUBSTR(gsm,4) AS integer), CAST(SUBSTR(gpl,4) AS integer), 
        title, description FROM gsm;""")
    c.executemany("INSERT INTO geo_sample VALUES (?,?,?,?);", iter(mc))

    def replace_organism(it, i):
        for row in map(list, it):
            taxon_id = taxon_map.get(row[i])
            if taxon_id is not None:
                row[i] = taxon_id
                yield row

    mc.execute("""SELECT CAST(SUBSTR(gsm,4) AS integer), 1, 
            source_name_ch1, molecule_ch1, characteristics_ch1, organism_ch1
            FROM gsm;""")
    c.executemany("INSERT INTO geo_channel VALUES (?,?,?,?,?,?);", 
            replace_organism(iter(mc), 5))

    mc.execute("""SELECT CAST(SUBSTR(gsm,4) AS integer), 2, 
                source_name_ch2, molecule_ch2, characteristics_ch2, organism_ch2
            FROM gsm WHERE channel_count >= 2;""")
    c.executemany("INSERT INTO geo_channel VALUES (?,?,?,?,?,?);", 
            replace_organism(iter(mc), 5))

    mc.execute("""SELECT CAST(SUBSTR(gse,4) AS integer), CAST(SUBSTR(gsm,4) AS integer) 
        FROM gse_gsm;""")
    c.executemany("INSERT INTO geo_series_sample VALUES (?,?);", iter(mc))
 
##################
# Public functions
##################

def ensure_indexes():
    db = connect()
    c = db.cursor()

    LOG.debug("building indexes")
    with open(fs.join(fs.dirname(__file__), "index.sql")) as h:
        indexes = h.read()
    c.executescript(indexes)
    c.close()
    db.commit()
 
def initialize(db):
    c = db.cursor()
    with open(fs.join(fs.dirname(__file__), "schema.sql")) as h:
        schema = h.read()
    c.executescript(schema)
    c.close()
    db.commit()

    initialize_core(db)
    initialize_ontologies(db) 

    c = db.cursor()
    c.execute("SELECT name,id FROM taxon;")
    taxon_map = dict(c)
    initialize_geo(db, taxon_map)

    c.close()
    db.commit()

    # indexes
    ensure_indexes()
    
    LOG.info("Local NCBI database initialization complete")

def connect(path=fs.join(CONFIG["cache"]["root"], "hta.db"), read_only=True):
    if not fs.exists(path):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        cx = sqlite3.connect(path)
        initialize(cx)
    #if read_only is True:
    #    uri = "file://{}?mode=ro".format(path)
    #    return sqlite3.connect(uri, uri=True)
    return sqlite3.connect(path)

def query(sql):
    cx = connect()
    try:
        o = pd.read_sql(sql, cx)
        if o.shape[1] == 1:
            return list(o.iloc[:,0])
        else:
            return o
    finally:
        cx.close()

def update_geo_labels():
    db = connect(read_only=False)
    LOG.info("updating GEO labels")

    c = db.cursor()
    c.execute("""
        SELECT term.key,term.id 
        FROM term 
        INNER JOIN ontology
        ON ontology.id=term.ontology_id
        WHERE ontology.key='BTO';""")
    tmap = dict(c)

    import hta.data.ncbi.geo.annotation
    import itertools

    def records():
        for row in hta.data.ncbi.geo.annotation.annotate():
            if isinstance(row[2], int):
                row = list(row)
                row[2] = tmap.get("BTO:{tissue_id:07d}".format(tissue_id=row[2]))
            if any(row[2:]):
                yield row

    c = db.cursor()
    c.execute("DELETE FROM geo_label;")
    db.commit()

    c = db.cursor()
    c.executemany("""INSERT INTO geo_label (geo_sample_id, channel, tissue_id, gender, age) 
            VALUES (?,?,?,?,?);""", records())
    db.commit()
