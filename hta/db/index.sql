CREATE INDEX IF NOT EXISTS ix_gene_taxon_id 
    ON gene(taxon_id);
CREATE INDEX IF NOT EXISTS ix_gene_accession_gene_id 
    ON gene_accession(gene_id);

CREATE INDEX IF NOT EXISTS ix_gene_symbol
    ON gene(symbol);
CREATE INDEX IF NOT EXISTS ix_gene_symbol_ci
    ON gene(symbol COLLATE NOCASE);

CREATE INDEX IF NOT EXISTS ix_term_ontology_id 
    ON term (ontology_id);
CREATE INDEX IF NOT EXISTS ix_term_key 
    ON term (key);
CREATE INDEX IF NOT EXISTS ix_term_name
    ON term (name);
CREATE INDEX IF NOT EXISTS ix_term_term_agent_id 
    ON term_term (agent_id);
CREATE INDEX IF NOT EXISTS ix_term_term_target_id 
    ON term_term (target_id);
CREATE INDEX IF NOT EXISTS ix_term_closure_ancestor_id 
    ON term_closure (ancestor_id);
CREATE INDEX IF NOT EXISTS ix_term_closure_descendant_id 
    ON term_closure (descendant_id);
CREATE INDEX IF NOT EXISTS ix_term_synonym 
    ON term_synonym (term_id);

CREATE INDEX IF NOT EXISTS ix_gene_term_gene_id
    ON gene_term (gene_id);
CREATE INDEX IF NOT EXISTS ix_gene_term_term_id
    ON gene_term (term_id);

CREATE INDEX IF NOT EXISTS ix_geo_series_geo_series_id 
    ON geo_series_sample (geo_series_id);
CREATE INDEX IF NOT EXISTS ix_geo_series_geo_sample_id 
    ON geo_series_sample (geo_sample_id);
CREATE INDEX IF NOT EXISTS ix_geo_label_geo_sample_id 
    ON geo_label (geo_sample_id, channel);
CREATE INDEX IF NOT EXISTS ix_geo_channel_geo_sample_id 
    ON geo_channel (geo_sample_id);
CREATE INDEX IF NOT EXISTS ix_geo_sample_geo_platform_id 
    ON geo_sample (geo_platform_id);

CREATE INDEX IF NOT EXISTS ix_geo_label_gender
    ON geo_label (gender);
CREATE INDEX IF NOT EXISTS ix_geo_label_tissue
    ON geo_label (tissue_id);
