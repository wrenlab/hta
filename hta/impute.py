import math

import pandas as pd
import numpy as np

def knn(X, k=None):
    """
    Impute missing values using mean of KNN.

    Arguments
    ---------
    X: :class:`pandas.DataFrame`
        The expression matrix, with probes as rows and samples as columns.

    Returns
    -------
    A :class:`pandas.DataFrame` with missing values imputed, and rows containing
    unimputable values dropped. The resulting :class:`pandas.DataFrame` will
    have no missing values and may have fewer rows.
    """
    assert isinstance(X, pd.DataFrame)
    if k is None:
        k = math.ceil(X.shape[1] * 0.1)
    assert isinstance(k, int)

    drop_threshold = int(0.25 * X.shape[1])
    X = X.dropna(axis=0, thresh=drop_threshold)
    nn = X.corr().apply(lambda r: r.order(ascending=False).index).iloc[1:,:].T
    Xn = X.copy()
    for i,j in zip(*np.array(X.isnull()).nonzero()):
        mu = X.iloc[i,:].loc[nn.iloc[j,:]].dropna().iloc[:k].mean()
        Xn.iloc[i,j] = mu
    return Xn.dropna(axis=0)
