"""
Meta-analysis methods for analyzing multiple datasets and summarizing results.
"""

import multiprocessing as mp

import pandas as pd
import numpy as np
from scipy import stats
import seaborn as sns

import hta.expression
import hta.data.ncbi
from hta.util import LOG

def fisher_method(pvs):
    x2 = -2 * pvs.apply(np.log).sum()
    df = 2 * len(pvs) 
    return stats.chi2.sf(x2, df)

def _correlate(args):
    X, (platform_id, series_id), covariate, min_size, normalize = args

    n = len(set(covariate.index) & set(X.index))
    if n < min_size:
        LOG.info("{} {} skipped because N={} < {} (minimum)"\
                .format(platform_id, series_id, n, min_size))
        return
    if normalize:
        X = hta.expression.quantile_normalize(X)
    r = X.corrwith(covariate)
    return r, (series_id, platform_id), pd.Series({"N": n})

def correlate(selection, key, normalize=True, min_size=10):
    # FIXME ? 
    # for now, if key is string, covariate is label,
    # if int, it is interpreted as a gene
    rows = []
    index = []
    meta = []

    def jobs():
        for X in selection:
            try:
                if isinstance(key, int):
                    covariate = X.loc[key,:]
                elif isinstance(key, str):
                    covariate = selection._A[key].loc[X.index].dropna()
            except Exception as e:
                #print(str(e))
                LOG.info(str(e))
                return
            yield X, (X.platform_id, X.series_id), covariate, min_size, normalize

    pool = mp.Pool()
    for rs in pool.imap_unordered(_correlate, jobs()):
        if rs is None:
            continue
        r, ix, m = rs
        rows.append(r)
        index.append(ix)
        meta.append(m)
               
    index = pd.MultiIndex.from_tuples(index, names=("Series ID", "Platform ID"))
    o = pd.DataFrame.from_records(rows, index=index).T
    o.columns.meta = pd.DataFrame(meta, index=index)
    return o

def correlation_report(selection, *args, taxon_id=9606, **kwargs):
    """
    Perform a meta-analysis of correlation coefficient between a covariate
    and gene expression.

    Returns
    -------
    A :class:`pandas.DataFrame` with genes as rows and meta-analytic summary
    parameters as columns. The parameters are:
    - Index: Entrez Gene ID
    - Symbol, Name: Gene symbol, name
    - Consistency: the ratio of "up" or "down" experiments (whichever is most frequent)
        to all experiments (thus, range is 0.5-1.0).
    - Mean_r: mean correlation coefficient
    - Median_r: median correlation coefficient
    - P_Quantile: quantile of p-value 
        (0 = gene with best "up" p-value, 1=gene with best "down" p-value)
    - P-Value: meta-analytic p-value using Fisher's method

    Additionally, the returned object has an attribute "correlations", which shows
    the raw Pearson correlation between gene and covariate for each 
    gene-experiment combination.
    """
    r = correlate(selection, *args, **kwargs)

    n_ex = r.apply(lambda x: (~x.isnull()).sum(), axis=1)
    n_ex.name = "N"
    
    cons = (r > 0).sum(axis=1) / n_ex
    cons = pd.Series(np.maximum(cons, 1-cons), index=cons.index)\
            .apply(lambda x: round(x,3))
    cons.name = "Consistency"
    
    r_mu = r.mean(axis=1).apply(lambda x: round(x,3))
    r_mu.name = "Mean_r"
    
    r_median = r.apply(np.median, axis=1).apply(lambda x: round(x,3))
    r_median.name = "Median_r"
    
    # Meta-analytic p-value with Fisher's method
    n = r.columns.meta["N"]
    t = r * ((n - 2) / (1 - r ** 2)).apply(np.sqrt)
    #sign = np.sign(t)
    #p = 2 * np.minimum(p, 1-p)
    p = stats.t.sf(t, n)
    p = pd.DataFrame(p, index=r.index, columns=r.columns)
    mp = p.apply(fisher_method, axis=1)
    mp.name = "P-Value"
    
    # Mean rank quantile
    #rank = r.apply(lambda x: pd.Series(np.arange(len(x)), 
    #   index=x.order(ascending=False).index)).mean(axis=1).order()
    #rank /= len(r)
    #rank.name = "Rank Quantile"
    
    # P-value rank
    rank = (pd.Series(np.arange(len(mp)), index=mp.order().index) \
            / len(mp)).apply(lambda x: round(x,3))
    rank.name = "P_Quantile"
    
    #sns.distplot(rp["P-Value"])

    genes = hta.data.ncbi.Entrez.genes(taxon_id)
    o = genes\
        .join(n_ex, how="outer")\
        .join(cons, how="outer")\
        .join(r_mu, how="outer")\
        .join(r_median, how="outer")\
        .join(rank, how="outer")\
        .join(mp, how="outer")\
        .dropna(subset=["Symbol"])\
        .sort("P-Value")
        #.dropna()
    o.correlations = genes.join(r)
    o.correlations.columns = list(o.correlations.columns)[:2] + \
            ["-".join(list(reversed(c))) for c in r.columns]
    return o
