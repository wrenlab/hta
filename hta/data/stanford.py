import gzip
import urllib.parse

import pandas as pd

import hta.util

def ailun(accession):
    """
    Fetch AILUN probe-gene mappings.

    Returns dict of probe ID -> Entrez Gene ID.
    """
    assert accession.startswith("GPL")

    base_url = "ftp://ailun.stanford.edu/ailun/annotation/geo/"
    url = urllib.parse.urljoin(base_url, "{}.annot.gz".format(accession))
    path = hta.util.download(url)

    return dict([(str(k),v) for k,v in pd.read_csv(path, sep="\t", compression="gzip", header=False)\
            .iloc[:,:2]\
            .to_records(index=False)])
