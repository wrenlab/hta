"""
Tools for managing reads and alignment data from NCBI SRA.
"""

import io
import time
import os
from os.path import expanduser, abspath, dirname, splitext, basename
import subprocess as sp
import urllib.parse
import gzip
import multiprocessing as mp
import shutil
import tempfile
from itertools import islice, groupby
from collections import namedtuple

import pysam
import pyfasta
import pandas as pd

import hta.util
from hta.util import LOG
from hta.util import fs
import hta.util.aspera

Region = namedtuple("Region", "contig,start,end,name,strand")

class SRAMetaDB(hta.util.CachedDB):
    URL = "http://gbnci.abcc.ncifcrf.gov/backup/SRAmetadb.sqlite.gz"

    def __init__(self):
        super(SRAMetaDB, self).__init__()

def get_assembly_list():
    ASSEMBLY_LIST_URL = \
            "ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_REPORTS/assembly_summary_refseq.txt"
    return pd.read_csv(hta.util.download(ASSEMBLY_LIST_URL), 
            sep="\t").set_index(["# assembly_accession"])

class Assembly(object):
    """
    A genome assembly, comprising the genome assembly, sequence, 
    corresponding transcript loci, and associated aligner indices.

    Also contains accessors for data from samples aligned to this 
    assembly.
    """
    def __init__(self, db, key):
        self._db = db
        self._root = db._root
        self._key = key
        if not fs.exists(self.sequence_path):
            raise Exception("{}: Assembly does not exist".format(self._key))

    @staticmethod
    def ensure(db, key):
        """
        Return an Assembly object, downloading and initializing it if necessary.
        """
        try:
            return Assembly(db, key)
        except:
            root = db._root
            assembly_list = get_assembly_list()
            accession = assembly_list.ix[assembly_list["asm_name"] == key,:]\
                    .index[0]
            meta = assembly_list.loc[accession,:]
            remote = urllib.parse.urlparse(meta["ftp_path"]).path
            prefix = os.path.basename(remote)

            genome_uri = fs.join(remote, "{}_genomic.fna.gz".format(prefix))
            genome_target = fs.join(root, "assembly", "{}.fna.gz".format(key))
            genome_target_dc = db._assembly_path(key)
            if not fs.exists(genome_target_dc):
                db._ascp.download(genome_uri, genome_target)
                with gzip.open(genome_target, "rt") as i:
                    with open(genome_target_dc, "wt") as o:
                        for line in i:
                            o.write(line)
                    fs.rm(genome_target)

            gff_uri = fs.join(remote, "{}_genomic.gff.gz".format(prefix))
            gff_target = fs.join(root, "assembly", "{}.gff.gz".format(key))
            if not fs.exists(fs.join(root, "assembly", "{}.gff".format(key))):
                db._ascp.download(gff_uri, gff_target)
                sp.check_call(["gunzip", gff_target])

            o = Assembly(db, key)
            o.index()
            return o

    def index(self):
        """
        Index this assembly's sequence on the hardcoded aligners 
        (currently Bowtie2 and Tophat2).
        """
        prefix = fs.join(self._root, "assembly", self.key)
        fna_path = fs.join(self._root, "assembly", "{}.fa".format(self.key))
        gff_path = "{}.gff".format(prefix)
        tx_prefix = "{}.tx".format(prefix)
        assert fs.exists(fna_path)
        assert fs.exists(gff_path)

        pwd = os.getcwd()
        os.chdir(os.path.dirname(fna_path))

        try:
            if not fs.exists(fs.join(self._root, "assembly", "{}.1.bt2".format(self.key))):
                LOG.info("{} : Building bowtie 2 index".format(self.key))
                sp.check_call(["bowtie2-build", fna_path, self.key])

            if not fs.exists("{}.1.bt2".format(tx_prefix)):
                LOG.info("{} : Building tophat 2 index".format(self.key))
                sp.check_call(["tophat2", "-G", gff_path, 
                    "--transcriptome-index", tx_prefix, prefix])
                if fs.exists("tophat_out"):
                    shutil.rmtree("tophat_out")
        except Exception as e:
            raise e
        finally:
            os.chdir(pwd)

    @property
    def key(self):
        return self._key

    @property
    def sequence_path(self):
        return fs.join(self._root, "assembly", "{}.fa".format(self._key))

    @property
    def sequence(self):
        return pyfasta.Fasta(self.sequence_path)

    @property
    def genome_path(self):
        return fs.join(self._root, "assembly", "{}.genome".format(self._key))

    @property
    def genome(self):
        path = self.genome_path
        if not fs.exists(path):
            fa = self.sequence
            genome = dict([(k.split()[0], len(fa[k])) for k in fa.keys()])
            with open(path, "w") as h:
                for k,length in sorted(genome.items()):
                    print(k, length, sep="\t", file=h)

        o = {}
        with open(path) as h:
            for line in h:
                k, length = line.rstrip("\n").split("\t")
                length = int(length)
                o[k] = length
        return o

    def transcripts(self, type="exon"):
        assert type in ("exon", "gene", "region")

        gff_path = fs.join(self._root, "assembly", "{}.gff".format(self.key))
        bed_path = fs.join(self._root, "assembly", "{}.bed".format(self.key))
        assert fs.exists(gff_path)
        if not fs.exists(bed_path):
            LOG.info("{} : Converting GFF to BED".format(self.key))
            with open(gff_path, "rb") as hi:
                with open(bed_path, "wb") as ho:
                    sp.check_call(["gff2bed"], stdin=hi, stdout=ho)

        LOG.debug("{} : Loading transcripts from BED".format(self.key))
        rows = pd.read_csv(bed_path, sep="\t", header=None)
        rows.columns = ["contig", "start", "end", "id", "score", "strand",
                "source", "type", "_", "attrs"]
        rows = rows.ix[rows["type"] == type,:]

        def get_gene_id(s):
            o = None
            for kv in s.split(";"):
                if kv.startswith("Dbxref"):
                    xrefs = dict([_.split(":",1) for _ in kv.split("=",1)[1].split(",")])
                    o = xrefs.get("GeneID")
                    if o is not None:
                        o = int(o)
            return o

        rows["Gene ID"] = rows["attrs"].apply(get_gene_id)
        return rows.loc[:,["id", "contig", "start", "end", "strand", "Gene ID"]]\
                .set_index(["Gene ID", "id"])\
                .dropna()

    def transcripts_path(self, strand, type="exon"):
        assert type == "exon"

        path = fs.join(self._root, "assembly", 
                "{}.{}.{}.bed".format(self.key, type, strand))
        if not fs.exists(path):
            o = self.transcripts(type=type)
            o = o.ix[o["strand"] == strand,:]
            gene_id, exon_id = list(zip(*o.index.values))
            o["name"] = exon_id
            o["score"] = "0"
            o = o.loc[:,["contig", "start", "end", "name", "score", "strand"]]
            o.to_csv(path, sep="\t", index=False, header=False)
        return path
 
    @property
    def alignments(self):
        aln_dir = fs.join(self._root, "alignment", self._key)
        for path in fs.ls(aln_dir):
            if path.endswith(".bam"):
                fname = basename(path)
                run_id = splitext(fname)[0]
                yield Alignment(self, run_id)

    @property
    def pileups(self):
        root = fs.join(self._root, "pileup", self.key)
        for path in fs.ls(fs.join(root, "+")):
            id = splitext(basename(path))[0]
            if fs.exists(fs.join(root, "-", "{}.bw".format(id))):
                yield Pileup(self, id)

    def ensure_pileups(self):
        """
        Ensure that all alignments have corresponding pileups.
        """

    def align(self, run_id):
        aligner = "bowtie2"

        fq_1 = fs.join(self._root, "reads", "{}_1.fastq.gz".format(run_id))
        fq_2 = fs.join(self._root, "reads", "{}_2.fastq.gz".format(run_id))
        fq_u = fs.join(self._root, "reads", "{}.fastq.gz".format(run_id))
        prefix = fs.join(self._root, "assembly", "{}".format(self.key))
        if fs.exists(fq_1) and fs.exists(fq_2):
            is_paired = True
        elif fs.exists(fq_u):
            is_paired = False
        else:
            raise Exception("{} : No reads".format(run_id))
        assert fs.exists("{}.1.bt2".format(prefix))
        ncpu = mp.cpu_count()

        p_bam = fs.join(self._root, "alignment", self.key, "{}.bam".format(run_id))
        p_sbam = fs.join(self._root, "alignment", self.key, "{}.sorted.bam".format(run_id))
        p_report = fs.join(self._root, "alignment", self.key, "{}.report".format(run_id))
        os.makedirs(dirname(p_bam), exist_ok=True)

        if fs.exists(p_bam):
            LOG.info("{} : already aligned to {}".format(run_id, self.key))
            return

        with open(p_bam, "wb") as handle, open(p_report, "wb") as report:
            LOG.info("{} : aligning with {}".format(run_id, aligner))
            cmd = ["bowtie2", "-p{}".format(ncpu-2), "--mm", "-x", prefix]
            if is_paired:
                cmd.extend(["-1", fq_1, "-2", fq_2])
            else:
                cmd.extend(["-U", fq_u])
            p1 = sp.Popen(cmd, stdout=sp.PIPE, stderr=report)
            p2 = sp.Popen(["samtools", "view", "-bS", "-"], stdin=p1.stdout, 
                    stdout=handle)
            p2.wait()
            
        LOG.info("{} : sorting BAM".format(run_id))
        try:
            sp.check_call(["samtools", "sort", 
                "-T", "{}.{}".format(run_id, os.getpid()),
                "-O", "bam",
                "-o", p_sbam,
                "-@", str(ncpu-2), 
                p_bam])
        except sp.CalledProcessError as e:
            fs.rm(p_sbam, force=True)
            LOG.error("{} : error sorting BAM".format(run_id))
            raise e

        fs.mv(p_sbam, p_bam)
        LOG.info("{} : indexing BAM".format(run_id))

        sp.check_output(["samtools", "index", p_bam])
        LOG.info("{} : alignment OK".format(run_id))

        # FIXME: --library-type
        #with tempfile.TemporaryDirectory() as wd:
            #cmd = [ "tophat2", 
            #        "-T",
            #        "-o", "test",#wd,
            #        "-p{}".format(ncpu-2), 
            #        "--no-novel-juncs", "--no-novel-indels",
            #        "--transcriptome-index", tx_prefix, prefix, fq ]
                        #target = fs.join(self._root, "alignment", 
                        #assembly, "{}.bam".format(run_id))
            #os.makedirs(os.path.dirname(target), exist_ok=True)
            #shutil.move(fs.join(wd, "accepted_hits.bam"), target) 

    def pileup(self, run_id):
        return Pileup(self, run_id)

    def expression(self, collapse=True):
        o = {}
        root = self._root
        def jobs():
            it = iter(self.pileups)
            #it = islice(it, 3)
            for pu in it:
                yield (root, self.key, pu._id)
        pool = mp.Pool()
        o = {}
        for run_id, rs in pool.imap_unordered(_pileup_expression, jobs()):
            o[run_id] = rs
        o = pd.DataFrame.from_dict(o)

        if collapse:
            transcripts = self.transcripts()
            exon_gene = dict(list(map(reversed, transcripts.index.values)))
            o = o.groupby(exon_gene).mean()
            o.index.name = "Gene ID"
        return o

class Sample(object):
    def __init__(self):
        pass

class AlignedSample(object):
    def __init__(self):
        pass

def _pileup_expression(args):
    root, assembly_id, run_id = args
    db = SRA(root)
    assembly = db.assembly(assembly_id)
    pileup = assembly.pileup(run_id)
    return (run_id, pileup.expression()["mean"])

class Pileup(object):
    def __init__(self, assembly, run_id):
        self._id = run_id
        self._assembly = assembly
        self._root = fs.join(assembly._root, "pileup", assembly._key)
        assert fs.exists(self._path("+"))
        assert fs.exists(self._path("-"))

    def __repr__(self):
        return "<Pileup '{}/{}'>".format(self._assembly.key, self._id)

    def _path(self, strand):
        assert strand in ("+","-")
        return fs.join(self._root, strand, "{}.bw".format(self._id))

    def expression(self):
        strand = "+"

        o = {}
        for strand in ["+","-"]:
            LOG.debug("{}/{}/{} : quantifying with Kent utilities"\
                    .format(self._assembly.key, self._id, strand))
            rs = sp.check_output(["bigWigAverageOverBed", self._path(strand), 
                self._assembly.transcripts_path(strand, type="exon"),
                "/dev/stdout"], stderr=sp.DEVNULL)
            rs = io.BytesIO(rs)
            o[strand] = pd.read_csv(rs, sep="\t", header=None, 
                    names=["name", "size", "covered", "sum", "mean0", "mean"])
        o = pd.concat(list(o.values())).set_index(["name"])
        o.name = self._id
        return o

class Alignment(object):
    def __init__(self, assembly, id):
        self._id = id
        self._assembly = assembly
        self._path = fs.join(assembly._root, "alignment", assembly._key, "{}.bam".format(id))
        if not fs.exists(self._path):
            raise Exception("{} : alignment doesn't exist".format(id))

    def __repr__(self):
        return "<Alignment: {}/{}>".format(self._assembly.key, self._id)

    def _ensure_index(self):
        if not fs.exists("{}.bai".format(self._path)):
            LOG.info("{} : indexing BAM".format(self._id))
            sp.check_output(["samtools", "index", self._path])

    def _ensure_pileup(self):
        for strand in ("+","-"):
            path = self.pileup_path(strand)
            if not fs.exists(path):
                os.makedirs(dirname(path), exist_ok=True)
                self.toBigWig(path, strand=strand)

    def pileup_path(self, strand):
        assert strand in ("+", "-")
        return fs.join(self._assembly._root, "pileup", self._assembly._key,
                strand, "{}.bw".format(self._id))
    
    @property
    def regions(self):
        # FIXME: use bedtools bamtobed
        self._ensure_index()
        aln = pysam.AlignmentFile(self._path, "rb")
        genome = self._assembly.genome

        for contig, contig_length in genome.items():
            for read in aln.fetch(contig, 0, contig_length):
                if read.is_secondary or read.is_unmapped:
                    continue

                fwd_strand = not read.is_reverse 
                if read.is_read2:
                    fwd_strand = not fwd_strand
                strand = "+" if fwd_strand else "-"

                start = read.reference_start
                end = read.reference_end
                if not ((start is not None) and (end is not None)):
                    continue
                start, end = min(start, end), max(start, end)

                #contig_id = read.reference_id
                #if contig_id not in contigs:
                #    contigs[contig_id] = aln.getrname(contig_id)
                #contig = contigs[contig_id]

                yield Region(contig, start, end, None, strand)
    
    def toBED(self, path, strand=None):
        assert strand in ("+","-",None)
        selected_strand = strand
        with tempfile.NamedTemporaryFile(mode="wt") as tmp:
            regions = self.regions
            #regions = islice(regions, 500)
            for contig, start, end, _, strand in regions:
                if (selected_strand is not None) and (strand != selected_strand):
                    continue
                print(contig, start, end, ".", "0", strand, sep="\t", file=tmp)
            tmp.flush()
            with open(path, "wb") as o:
                sp.check_call(["bedtools", "sort", "-i", tmp.name], stdout=o)

    def toBEDGraph(self, path, strand="+"):
        assert strand in ("+","-")
        with tempfile.NamedTemporaryFile() as tmp:
            self.toBED(tmp.name, strand=strand)
            with open(path, "wb") as o:
                sp.check_call(["bedtools", "genomecov", "-bg", "-strand", strand, 
                    "-g", self._assembly.genome_path,
                    "-i", tmp.name], stdout=o)

    def toBigWig(self, path, strand="+"):
        assert strand in ("-", "+")
        LOG.info("{}/{}/{} : indexing BigWig pileup".format(self._assembly.key, 
            self._id, strand))
        with tempfile.NamedTemporaryFile() as tmp:
            self.toBEDGraph(tmp.name, strand=strand)
            sp.check_call(["bedGraphToBigWig", tmp.name, 
                self._assembly.genome_path,
                path])

    def query(self, regions):
        self._ensure_pileup()
        key_fn = lambda rg: rg.strand
        #pileups = dict([self.pileup_path(strand) for strand in ("+","-")])
        for strand, regions in groupby(sorted(regions, key=key_fn), key=key_fn):
            pass

class SRA(object):
    """
    Directory structure:
    $root
        /reads
            /sra
            /fq
        /assembly
            /$name - FASTA files, GFFs, and Bowtie indices are prefixed by $assembly
        /alignment
            /$assembly
                /${read accession}.{bam,bai}
        /pileup
            /{+,-}
                /$accession.bw
    """
    BASE_URI = "/sra/sra-instant/reads/ByRun/sra/"

    def __init__(self, root):
        self._root = abspath(expanduser(root))
        os.makedirs(self._root, exist_ok=True)

        self._ascp = hta.util.aspera.Client("anonftp", "ftp-trace.ncbi.nlm.nih.gov")

        os.makedirs(fs.join(self._root, "assembly"), exist_ok=True)
        os.makedirs(fs.join(self._root, "reads"), exist_ok=True)

    #def _fastq_path(self, run_id):
    #    return fs.join(self._root, "reads", "{}.fastq.gz".format(run_id))

    def assembly(self, key):
        return Assembly.ensure(self, key)

    def alignment(self, assembly, run_id):
        return Alignment(self.assembly(assembly), run_id)

    def _assembly_path(self, key):
        return fs.join(self._root, "assembly", "{}.fa".format(key))

    def ensure_runs(self, run_ids, keep_sra_file=False):
        # TODO: use mp dummy pool: http://stackoverflow.com/questions/14533458/python-threading-multiple-bash-subprocesses
        for run_id in run_ids:
            assert run_id[:3] in ("SRR", "ERR", "DRR")

        exe = hta.util.which("fastq-dump")
        target_dir = fs.join(self._root, "reads")
        processes = []

        for run_id in run_ids:
            uri = fs.join(self.BASE_URI, run_id[:3], run_id[:6], 
                    run_id, "{}.sra".format(run_id))
            sra_path = fs.join(target_dir, "{}.sra".format(run_id))

            if not fs.exists(sra_path):
                try:
                    self._ascp.download(uri, target_dir)
                except sp.CalledProcessError:
                    LOG.warn("{}: Aspera download failed".format(run_id))
                    continue

            paired_exists = fs.exists(fs.join(target_dir, "{}_1.fastq.gz".format(run_id)))
            unpaired_exists = fs.exists(fs.join(target_dir, "{}.fastq.gz".format(run_id)))
            if not (paired_exists or unpaired_exists):
                LOG.info("{}: Exporting SRA format to FASTQ".format(run_id))
                p = sp.Popen([exe, "--split-3", "--gzip", "-O", target_dir, sra_path])
                processes.append(p)

        while True:
            time.sleep(5)
            for p in processes:
                p.poll()
            if all([(p.returncode is not None) for p in processes]):
                break
        LOG.info("{} runs exported to FASTQ".format(len(processes)))

        if not keep_sra_file:
            for run_id in run_id:
                sra_path = fs.join(target_dir, "{}.sra".format(run_id))
                if fs.exists(sra_path):
                    LOG.debug("Removing {}".format(sra_path))
                    fs.rm(sra_path)
