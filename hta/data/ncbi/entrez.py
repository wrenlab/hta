import os
import sqlite3
from collections import namedtuple, defaultdict

import pandas as pd

import hta.db
from hta.util import fs

GEOSample = namedtuple("GEOSample",
        ["accession", "channel", "experiments", "taxon_id",
            "platform", "title", "description", "characteristics", "source"])
GEOSample.text = property(lambda s: "\n".join(map(lambda x: x or "", 
    [s.title, s.description, s.characteristics, s.source])))

GEOExperiment = namedtuple("GEOExperiment",
        ["accession", "title", "summary", "design"])
GEOExperiment.text = property(lambda s: "\n".join(map(lambda x: x or "", 
    [s.title, s.summary, s.design])))

class Entrez(object):
    """
    A singleton wrapping a SQLite database containing data from NCBI (genes, taxa, etc.).
    """
    _instance = None

    def __init__(self):
        self._cx = hta.db.connect()

    @staticmethod
    def _get_instance():
        if Entrez._instance is None:
            Entrez._instance = Entrez()
        return Entrez._instance

    @staticmethod
    def query(sql):
        ez = Entrez._get_instance()
        return pd.read_sql(sql, ez._cx)

    @staticmethod
    def gene(gene_id):
        assert isinstance(gene_id, int)
        ez = Entrez._get_instance()
        return pd.read_sql("""
            SELECT id as 'Entrez Gene ID', symbol as 'Symbol', name as 'Name'
            FROM gene WHERE id={}""".format(gene_id), ez._cx)\
                    .set_index(["Entrez Gene ID"])\
                    .iloc[0,:]

    @staticmethod
    def taxon(taxon_id):
        ez = Entrez._get_instance()
        return pd.read_sql("SELECT * FROM taxon WHERE id={}".format(taxon_id), ez._cx)\
                .iloc[0,:]

    @staticmethod
    def taxonomy():
        ez = Entrez._get_instance()
        o = pd.read_sql("""
            SELECT * FROM taxon""", ez._cx).set_index(["id"])
        o.index.name = "taxon_id"
        return o

    @staticmethod
    def genes(taxon_id):
        assert isinstance(taxon_id, int)
        ez = Entrez._get_instance()

        return pd.read_sql("""
            SELECT id as 'Entrez Gene ID', symbol as 'Symbol', name as 'Name'
            FROM gene
            WHERE taxon_id={}""".format(taxon_id), ez._cx)\
                    .set_index(["Entrez Gene ID"])

    @staticmethod
    def geo_samples():
        self = Entrez._get_instance()
        m = defaultdict(set)
        for gsm,gse in self.query("""SELECT geo_series_id,geo_sample_id 
            FROM geo_series_sample;""")\
                .to_records(index=False):
            m[gsm].add(gse)

        it = self.query("""
            SELECT s.id, c.channel, s.geo_platform_id, s.title, s.description,
                c.characteristics,c.taxon_id,c.source_name
            FROM geo_sample s
            INNER JOIN geo_channel c
                ON c.geo_sample_id=s.id
            WHERE 
            c.molecule='total RNA';""").to_records(index=False)
        for sample_id,i,platform_id,title,desc,ch,taxon_id,source in it:
            yield GEOSample(int(sample_id),int(i),m[gsm],int(taxon_id),
                    platform_id,title,desc,ch,source)

    @staticmethod
    def geo_experiments():
        self = Entrez._get_instance()
        for row in self.query("SELECT * FROM geo_series;").to_records(index=False):
            row = list(row)
            row[0] = int(row[0])
            yield GEOExperiment(*row)

if __name__ == "__main__":
    import sys

    #db = sqlite3.connect(sys.argv[1])
    #initialize_db(db)
    #print(next(Entrez.geo_samples()))
    import hta.db
    hta.db.update_geo_labels()
