from tarfile import TarFile
from tempfile import TemporaryDirectory
import gzip
import io
import itertools
import lxml.etree
import multiprocessing as mp
import os.path
import tarfile
import xml.etree.ElementTree as ET
import psutil

import pandas as pd

import hta.util.aspera
from hta.util import fs, LOG, as_float

def fetch(accession, target, compress_level=1):
    """
    Download the MiniML data associated with a GEO platform to the target path
    (creates a .tgz file). 

    Notes
    -----
    MiniML archives come in multiple parts, and the files inside are not
    sorted in any obvious way. To ease later processing stages, this code
    extracts all files from all the archives associated with the accession,
    then rearchives and recompresses them into a single archive. The downside
    is lots of disk space usage during processing.
    """
    assert isinstance(target, str)
    assert accession.startswith("GPL")

    LOG.info("{}: Fetching MiniML data".format(accession))
    target = os.path.abspath(target)

    aspera = hta.util.aspera.Client("anonftp", "ftp-private.ncbi.nlm.nih.gov")
    group = (accession[:-3] if len(accession) > 6 else "GPL") + "nnn"
    src = "/geo/platforms/{}/{}/miniml/".format(group, accession)

    with TemporaryDirectory() as dldir, TemporaryDirectory() as dcdir:
        aspera.download(src, dldir)

        idir = os.path.join(dldir, "miniml")
        files = [fname for fname in os.listdir(idir)
                if fname.endswith(".tgz")]

        LOG.debug("{}: Extracting MiniML tarballs".format(accession))
        for fname in files:
            path = os.path.join(idir, fname)
            tgz = tarfile.open(path, "r:gz")
            tgz.extractall(path=dcdir)
        
        LOG.debug("{}: Creating target archive".format(accession)) 
        with gzip.open(target, "w", compresslevel=1) as h:
            with TarFile(fileobj=h, mode="w") as oa:
                prepend = [
                        "{}_family.xml".format(accession),
                        "{}-tbl-1.txt".format(accession)]
                files = list(sorted(os.listdir(dcdir)))
                for fname in prepend:
                    oa.add(fs.join(dcdir, fname), arcname=fname)
                    files.remove(fname)

                for i,fname in enumerate(files):
                    path = fs.join(dcdir, fname)
                    oa.add(path, arcname=fname)
                    if (i != 0) and (i % 1000 == 0):
                        LOG.info("{}: Added file {}".format(accession, i))


def _parse_miniml_single(args):
    """
    Parse an individual sample in a MiniML archive (called in parallel
    by _parse_miniml).
    """
    accession, probe_ix, value_ix, ix, data = args
    with io.BytesIO(data) as h:
        try:
            X = pd.read_csv(h, sep="\t", usecols=(probe_ix, value_ix), header=None)
            if probe_ix > value_ix:
                X = X.iloc[:,(1,0)]
        except UnicodeDecodeError:
            LOG.warn("{} : UnicodeDecodeError".format(accession))
            return None
        ks = X.iloc[:,0].astype(str)
        try:
            vs = X.iloc[:,1].astype(float).values
        except ValueError:
            try:
                vs = list(map(as_float, X.iloc[:,1]))
            except ValueError:
                LOG.warn("{} : error parsing probe values as float".format(accession))
                return None
        try:
            values = pd.Series(vs, index=ks).loc[ix]
        except:
            LOG.warn("{} : no index values found".format(accession))
            return None
        if values.isnull().all():
            LOG.warn("{} : all probe values NaN".format(accession))
            return None
        values.name = accession
        LOG.debug("{} : OK".format(accession))
        return values


def parse(tar, parallel=True):
    """
    Parse a MiniML archive, lazily returning sample data.

    Parameters
    ----------
    tar: :class:`tarfile.TarFile` object

    Returns
    -------
    A generator of :class:`pandas.Series`, with data values usually representing 
    probe intensities ("VALUE" column in MiniML) and with the index corresponding
    to probe IDs.

    Notes
    -----
    This doesn't handle 2-color, will treat it like 1-color (it will return data
    for the 1st channel only).
    """
    it = iter(tar)

    # Get columns for probe ID and value from GPLnnn_family.xml
    ns = {"miniml": "http://www.ncbi.nlm.nih.gov/geo/info/MINiML"}
    txml = next(it)
    assert txml.name.startswith("GPL") and txml.name.endswith("_family.xml")
    platform_id = txml.name.split("_")[0]

    column_map = {}
    with tar.extractfile(txml) as h:
        parser = lxml.etree.XMLParser(recover=True)
        tree = ET.parse(h, parser=parser)
        root = tree.getroot()
        for sample in root.findall("miniml:Sample", ns):
            gsm = sample.attrib["iid"]
            table = sample.find("miniml:Data-Table", ns)
            if table is None:
                continue

            cm = {}
            for column in table.findall("miniml:Column", ns):
                i = int(column.attrib["position"]) - 1
                name = column.find("miniml:Name", ns)
                if name is None:
                    continue
                cm[name.text] = i

            probe_ix = cm.get("ID_REF")
            value_ix = cm.get("VALUE")
            if (probe_ix is not None) and (value_ix is not None):
                column_map[gsm] = (probe_ix, value_ix)

    LOG.debug("{} : {} GSMs with probe-column map".format(platform_id, len(column_map)))

    # Get list of probes from GPLnnn-tbl-1.txt
    ti = next(it)
    assert ti.name.startswith("GPL") and ti.name.endswith("-tbl-1.txt")
    accession = ti.name.split("-")[0]
    LOG.info("{}: Parsing MiniML data".format(accession))

    probes = []
    with tar.extractfile(ti) as th:
        for line in th:
            probe = line.decode("iso-8859-1").strip().split("\t")[0]
            probes.append(probe)

    ix = pd.Index(list(sorted(probes)))

    def iterator():
        # Parse individual GSMs
        for item in it:
            if not (item.name.startswith("GSM") and item.name.endswith("-tbl-1.txt")):
                continue
            gsm = item.name.split("-")[0]

            if not gsm in column_map:
                continue
            probe_ix, value_ix = column_map[gsm]

            with tar.extractfile(item) as h:
                data = h.read()
                yield (gsm, probe_ix, value_ix, ix, data)

    ncpu = min(mp.cpu_count() - 2, max(1, int((psutil.phymem_usage().total / 1e9) / 16)))
    if parallel:
        pool = mp.Pool(processes=ncpu)
        map_fn = pool.imap
    else:
        map_fn = map

    items = iterator()
    #items = itertools.islice(items, 500)
    for v in map_fn(_parse_miniml_single, items):
        if v is not None:
            yield v
