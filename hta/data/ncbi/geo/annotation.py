"""
"""
# TODO: if tissue maps to a term like "X cell line" and there is a term with different ID "X", 
#  use that and mark cell line status separately

import itertools
import collections
import re

import pandas as pd
import networkx as nx

from acfsa import Trie
import hta.ontology
from hta.util import LOG, memoize, dfstream
from hta.data.ncbi import Entrez
#from .metadata import GEOMetaDB
import multiprocessing as mp

############
# Annotation
############

BOUNDARY_CHARACTERS = '\n-._=,:\'; "/|{}()[]<>' \
        + ''.join([str(x) for x in range(0, 10)])

PATTERNS = {
    "age": "[^\w]age( *\((?P<age_unit1>[a-z]*)\))?:\
[ \t]*(?P<age>\d+[\.0-9]*)(( *\- *| (to|or) )\
(?P<age_end>\d+[\.\d]*))?([ \t]*(?P<age_unit2>[a-z]+))?",
    "age2": "(?P<age>\d+)(-(?P<age_end>\d+))*\s(?P<unit>(year|month|day))",
    "age_unit": "(age\s*unit[s]*|unit[s]* of age): (?P<age_unit>[a-z])",
    # Tissue (TODO: map to BTO)
    "tissue": "(cell type|tissue|organ) *: *(?P<tissue>[A-Za-z0-9\+\- ]+)",
    # Gender
    "gender": "(gender|sex) *: *(?P<gender>[A-Za-z]+)",
    # Disease states (TODO: map to DO)
    "cancer": "(tumor|tumour|cancer|sarcoma|glioma|leukem|mesothelioma|metastasis|carcinoma|lymphoma|blastoma|nsclc|cll|ptcl)",
    "control": "(control|normal|untreated)",
    "infection": "infec"
}
PATTERNS = dict((k, re.compile(v)) for k,v in PATTERNS.items())

# A common additional unit is "dpc", which refers to embryos. 
# Currently ignored.
# Some samples are labeled with something like "11 and 14 weeks". 
# I have no idea what this means, so it's ignored. 
TIME = {
        "year": 12,
        "y": 12,
        "yr": 12,
        "yrs": 12,
        "month": 1,
        "moth": 1,
        "mon": 1,
        "mo": 1,
        "m": 1,
        "week": 1 / 4.5,
        "wek": 1 / 4.5,
        "wk": 1 / 4.5,
        "w": 1 / 4.5,
        "day": 1 / 30,
        "d": 1 / 30,
        "hour": 1 / (24 * 30),
        "hr": 1 / (24 * 30),
        "h": 1 / (24 * 30)
}

AGE_MAX = {
        9606: 120 * 12,
        10090: 5 * 12,
        10116: 5 * 12
}

AGE_DEFAULT_UNIT = {
        9606: 12
}

GENDER = {
        "male": "M",
        "m": "M",
        "man": "M",
        "men": "M",
        "mal": "M",
        "boy": "M",
        "woman": "F",
        "women": "F",
        "female": "F",
        "f": "F",
        "fe": "M",
        "girl": "F",
}

GENDER_CODE = {
        "M": 1, "F": 0
}

def _search(pname, text, group=None):
    group = group or pname
    m = re.search(PATTERNS[pname], text)
    if m is not None:
        return m.group(group)

def _expand_synonym(text):
    o = set()
    o.add(text)
    if not text.endswith("s"):
        o.add(text+"s")
    words = text.split()
    if words[-1] == "cell":
        for s in _expand_synonym(" ".join(words[:-1])):
            o.add(s)
    #if text.endswith("cell line"):
    #    for s in _expand_synonym(text.rstrip(" cell line")):
    #        if s not in ["primary"]:
    #            o.add(s)
    return set([s for s in o if len(s) > 3])

def _is_disease_synonym(synonym):
    return any(w.endswith("oma") for w in synonym.split()) \
        or any(w.endswith("emia") for w in synonym.split()) \
        or any(w.endswith("plasia") for w in synonym.split()) \
        or "cancer" in synonym

def _is_invalid_term(synonym):
    invalid = ["culture condition", "infect"]
    return any(x in synonym for x in invalid)

@memoize
def get_samples():
    ix = ["sample_id", "channel"]
    records = Entrez.query("""
        SELECT s.id AS 'sample_id', c.channel, 
            s.geo_platform_id AS 'platform_id', 
            s.title, 
            s.description,
            c.characteristics,
            c.taxon_id,
            c.source_name AS 'source'
        FROM geo_sample s
        INNER JOIN geo_channel c
            ON c.geo_sample_id=s.id
        WHERE 
            c.taxon_id=9606 AND
            c.molecule='total RNA';""")\
                .drop_duplicates(ix)\
                .set_index(ix)
    records["text"] = \
            records.apply(lambda s: "\n".join(map(lambda x: x or "",
                [s.title, s.description, s.characteristics, s.source])), axis=1)
    return records

def extract_age(record):
    # FIXME: incorporate age_end
    age = None
    unit = None
    text = record.text.lower()
    m = re.search(PATTERNS["age"], text)
    if m is not None:
        unit_text = m.group("age_unit1") or m.group("age_unit2")
        #if unit_text is not None:
        #    self._age_unit_text.add(unit_text)

        if unit_text in ("packyears",):
            return
        unit = TIME.get(unit_text)
    else:
        m = re.search(PATTERNS["age2"], text)
        if m is not None:
            unit = TIME.get(m.group("unit"))

    if m is not None:
        age = float(m.group("age"))
        if m.group("age_end"):
            #FIXME: include these?
            age = (age + float(m.group("age_end"))) / 2
            #age = None
    else:
        if record.characteristics is not None:
            chs = record.characteristics.strip().lower().split(";")
            for ch in chs:
                if ch.startswith("age "):
                    m = re.search("(?P<age>\d+[\.0-9]*)", ch)
                    if m is not None:
                        age = float(m.group("age"))
                        break

    if age is None:
        if "age" in text.lower().split():
            print()
            print(record.accession)
            print(text)
        return
    if unit is None:
        if record.taxon_id == 9606:
            unit = 12
        else:
            return

    age *= unit

    # Remove ages too big
    # NB: This could also be done by postprocessing and fitting a per-species 
    # gompertz curve and cutting off at a probability OR determining max 
    # likelihood unit if absent
    if age:
        max_age = AGE_MAX.get(record.taxon_id, 1e10)
        if age >= max_age:
            return

    return age 

class Annotator(object):
    def __init__(self):
        self.tries = {}
        self.ontologies = {}
        self.graphs = {}

        self.ontologies["BTO"] = hta.ontology.fetch("BTO")
        self.graphs["BTO"] = self.ontologies["BTO"].to_graph()

        self.tries["BTO"] = Trie(case_sensitive=False, 
                allow_overlaps=False, 
                boundary_characters=" \t\n")
        for id, synonyms in self._tissue_synonyms().items():
            for s in synonyms:
                self.tries["BTO"].add(s, key=id)
        self.tries["BTO"].build()

        self._age_unit_text = set()

    def _tissue_synonyms(self):
        o = self.ontologies["BTO"]
        g = self.graphs["BTO"]
        depths = dict([(k,g.node[k].get("depth")) for k in g.nodes() if g.node[k].get("depth")])
        synonyms = collections.defaultdict(set)
        canonical = set(o.terms["Name"]) | set(o.synonyms["Synonym"])

        for id,name in zip(o.terms.index, o.terms["Name"]):
            if len(name) > 4:
                synonyms[id].add(name)
            synonyms[id] = synonyms[id] | set(s for s in _expand_synonym(name) if not s in canonical)
        for id,synonym in zip(o.synonyms.index, o.synonyms["Synonym"]):
            if len(synonym) > 4:
                synonyms[id].add(synonym)
            synonyms[id] = synonyms[id] | set(s for s in _expand_synonym(synonym) if not s in canonical)

        diseased = set()
        for id,ss in synonyms.items():
            if any(map(_is_disease_synonym, ss)):
                diseased.add(id)

        synonyms_o = collections.defaultdict(set)
        for id,ss in synonyms.items():
            if any(map(_is_invalid_term, ss)):
                continue
            try:
                path = nx.shortest_path(g, id, g.root)
            except nx.NetworkXNoPath:
                continue
            if id in diseased:
                for parent in path[1:]:
                    if parent in diseased:
                        id = parent
                        continue
                    #if g.edge[id][parent]["type"] != "develops_from":
                    #        break
                    #else:
                    #    id = parent
            synonyms_o[id] = synonyms_o[id] | ss
        return synonyms_o

    def extract_tissue(self, record):
        cell_line = False
        g = self.graphs["BTO"]
        tissue = None
        ms = self.tries["BTO"].search(record.text)
        if ms:
            # TODO: best way? deepest match, longest?
            ms.sort(key=lambda x: -g.node[x.key].get("depth", 0))
            #ms.sort(key=lambda x: x.start - x.end)
            m = ms[0]
            tissue = m.key
            text = record.text[m.start:m.end]
            if text.endswith("cell line"):
                cell_line = True
                ms = self.tries["BTO"].search(text.rstrip(" cell line"))
                if ms is not None and len(ms) == 1:
                    tissue = ms[0].key
        return tissue

    def extract_control(self, record):
        return bool(re.search(PATTERNS["control"], record.text.lower()))

    def extract_gender(self, record):
        text = record.text.lower()
        gender = GENDER_CODE.get(GENDER.get(_search("gender", text, "gender")))
        if record.characteristics and gender is None:
            #ch = "".join([s.title, s.characteristics]).lower()
            ch = record.characteristics.lower()
            f = bool(re.search(r"\bfemale\b", ch))
            m = bool(re.search(r"\bmale\b", ch))
            if f != m:
                if f:
                    gender = GENDER_CODE["F"]
                else:
                    gender = GENDER_CODE["M"]
        return gender

    def extract_age(self, record):
        return extract_age(record)

    def extract_pooled(self, record):
        text = record.text.lower()
        return True if bool(re.search(r"\bpool(ed)?\b", text)) else False

    def extract(self, record):
        pass

"""
def pelorus():
    import pelorus.data
    mdb = GEOMetaDB()

    m = dict(mdb.query("SELECT gsm,gpl FROM gsm;").to_records(index=False))
    o = pelorus.data.GEOMetadata(species_l=["human", "mouse", "rat"])

    for quality in ["origin", "age", "gender", "disease"]:
        o.add_text_mining_annotations(quality)
    #o["gsm"] = [a+"-"+str(c) for a,c in zip(o["gsm"], o["channel"])]
    a = o.annotations
    a = a.ix[a["channel"] == 1,:]
    a = a.pivot("gsm", "annotation_quality", "annotation")

    a["age"] = list(map(as_float, a["age"]))
    gender_map = {"male": 1, "female": 0}
    a["gender"] = [gender_map.get(x) for x in a["gender"]]
    a["platform_id"] = [m.get(x) for x in a.index]

    columns = list(a.columns)
    columns[columns.index("origin")] = "tissue"
    a.columns = columns
    return a
"""

Annotation = collections.namedtuple("Annotation", "sample_id,channel,tissue,gender,age")

def annotate(experiment_fallback=True):
    LOG.info("Annotating GEO labels")

    LOG.debug("Initializing annotator")
    annotator = Annotator()

    experiment_tissue = {}
    if experiment_fallback:
        LOG.debug("Extracting per-experiment fallback tissues")
        for e in Entrez.geo_experiments():
            tissue = annotator.extract_tissue(e)
            if tissue:
                experiment_tissue[e.accession] = tissue

    LOG.debug("Extracting per-channel labels")
    columns = ["sample_id", "channel", "tissue", "gender", "age", "pool", "control"]
    for record in Entrez.geo_samples(): #in pool.imap_unordered(_annotate_single, it):
        tissue = annotator.extract_tissue(record) #or experiment_tissue.get(s.accession) or ""
        pooled = int(annotator.extract_pooled(record))
        control = int(annotator.extract_control(record))
        gender = annotator.extract_gender(record)
        #gender = "" if gender is None else gender
        age = annotator.extract_age(record)

        row = [tissue, gender, age, pooled, control]
        if not any(row[:-2]):
            continue
        row = [record.accession, record.channel, tissue, gender, age]

        if experiment_fallback:
            if row[2] is None:
                for experiment_id in record.experiments:
                    tissue = experiment_tissue.get(experiment_id)
                    if tissue is not None:
                        row[2] = tissue
                        break
        if row[2] is not None:
            row[2] = int(row[2][4:])
        yield Annotation(*row)

if __name__ == "__main__":
    def m1(records):
        m = records["text"].str.extract(PATTERNS["age"])\
                .loc[:,["age","age_unit1","age_unit2"]]\
                .dropna(subset=["age"])\
                .dropna(subset=["age_unit1", "age_unit2"], how="all")
        age = m["age"].astype(float)
        unit_text = m["age_unit1"]
        ix = unit_text.isnull()
        unit_text.ix[ix] = m["age_unit2"].ix[ix]
        unit = unit_text.str.lower().apply(TIME.get)
        # if human
        if True:
            unit = unit.fillna(12)
        age *= unit
        print("M1:", len(age.dropna()), "/", records.shape[0])
        return age
    
    def m2(records):
        m = records["text"].str.extract(PATTERNS["age2"])\
                .loc[:,["age","unit"]]\
                .dropna(subset=["age"])
        unit = m["unit"].str.lower().apply(TIME.get)
        if True:
            unit = unit.fillna(12)
        age = m["age"].astype(float) * unit
        print("M2:", len(age.dropna()), "/", records.shape[0])
        return age

    def m3(records):
        def fn(chs):
            if chs is None:
                return
            chs = list(map(lambda x: x.strip(), chs.lower().split(";")))
            for ch in chs:
                if ch.startswith("age "):
                    m = re.search("(?P<age>\d+[\.0-9]*)", ch)
                    if m is not None:
                        return float(m.group("age"))
                        break
        age = records["characteristics"].apply(fn)
        print("M3:", len(age.dropna()), "/", records.shape[0])
        return age

    def m4(records):
        m = records["text"].str.lower()\
                .str.extract(r"(\bage\b(.+?)(?P<age>\d+(\.[0-9]+){0,1}))")
        return m["age"].dropna().astype(float)

    import seaborn as sns
    import numpy as np
    import matplotlib.pyplot as plt

    records = get_samples()
    age = m4(records)
    age.sort()
    print(age.shape)
    #print(age.head())
    print(age.describe())

    #sns.distplot(age)
    #plt.savefig("test.png")
    #print(age.head())
    #age = pd.DataFrame(np.empty((len(records),3)).fill(np.nan), 
    #        index=records.index, columns=["M1","M2","M3"])
    #age["M1"] = m1(records)
    #age["M2"] = m2(records)
    #age["M3"] = m3(records)

    #print(age.dropna(how="all").head())
    #print(age.dropna(how="all").shape)

    #ix = records["text"].str.lower().str.contains(r"\bage\b")
    #print(records.ix[ix,:]["text"].head())

    #age_m1 = m1(records)
    #age_m2 = m2(records)
    #age_m3 = m3(records)

    #annotator = Annotator()
    #pool = mp.Pool()
    #for age in pool.imap(extract_age, samples()):
    #    print(age)

        #if age:
        #    print(record.accession, record.channel, age)
#    annotate(n=10, experiment_fallback=False).dump(index=False)
    #dfs.dump(index=False)
    #annotate(experiment_fallback=False).head().to_csv(sys.stdout, sep="\t", index=False)
