import os
import random
import tarfile
import itertools
import multiprocessing as mp
from collections import namedtuple

import pandas as pd
import numpy as np
from scipy import stats

from h5df import Store

import hta.statistics
import hta.expression
import hta.data.stanford
import hta.analysis.ma
import hta.data.ncbi.geo.annotation
import hta.util
import hta.util.decorators
from hta.data.ncbi import Entrez
from hta.util import CONFIG, LOG, fs
from .metadata import GEOMetaDB
from hta.data.ncbi.geo import miniml

CorrelationResult = namedtuple("CorrelationResult", "summary,correlations,metadata")

class GEOSelection(object):
    """
    A reference to a subset of GEO sample data, allowing filtering by metadata
    label keys and performing meta-analyses on contained datasets.
    """

    def __init__(self, db, A):
        """
        Constructor. Do not call directly; used by :class:`GEO`. 
        Obtain a Selection by calling GEO.select().
        """
        self._db = db
        self._A = A

    ########
    # Helper
    ########

    def _subset(self, ix):
        """
        Return a new :class:`GEOSelection` by slicing labels using the given index.
        """
        return GEOSelection(self._db, self._A.ix[ix,:])

    #########
    # General
    #########

    def __len__(self):
        """
        Return the number of samples contained in this selection.
        """
        # FIXME: this may overestimate how many samples will actually
        # be returned by __iter__ because not all samples may be in the GEO database.
        return self._A.shape[0]

    def __iter__(self):
        """
        Return an iterator to GEO series matching the selection criteria.

        Returns
        -------
        A :class:`iter` of :class:`pandas.DataFrame`, one for each GSE. Only
        GSMs that are matched by this selection will be contained in each
        frame.
        """
        groups = dict([(gsm,(gpl,gse)) for (gsm,gse,gpl) in 
            self._db._metadb.query("""
            SELECT gsm.gsm,gse_gsm.gse,gsm.gpl 
            FROM gse_gsm 
            INNER JOIN gsm 
            ON gsm.gsm=gse_gsm.gsm;""")\
                    .to_records(index=False)])

        for (platform_id, series_id), pdata in self._A.groupby(groups):
            try:
                X = self._db.series(platform_id, series_id)
            except KeyError:
                continue
            ix = list(set(X.index) & set(pdata.index))
            X = X.loc[ix,:]
            X.platform_id = platform_id
            X.series_id = series_id
            X.labels = pdata.loc[ix,:]
            yield X
   
    ###########
    # Filtering
    ###########

    def filter(self, **kwargs):
        """
        Filter this selection by testing for equality or set membership against a
        scalar or string.

        Parameters
        ----------
        kwargs: a dict of label names to either a scalar (str, int) or a list
            If the value type is a scalar, annotations will be tested for equality.
            If the value type is a list, annotations will be tested to see if they
            are equal to any member of the list.

        Returns
        -------
        A new :class:`GEOSelection` object containing the subset of samples
        matching the regexes.

        Examples
        --------
        >> geo = hta.data.ncbi.GEO("geo.h5")
        >> q = geo.select()
        >> q1 = q.filter(Tissue="BTO:0000123")
        >> q2 = q.filter(platform_id=["GPL96","GPL570"])
        """
        kwargs = list(kwargs.items())
        k,v = kwargs[0]
        if isinstance(v, list):
            ix = self._A[k].isin(v)
        else:
            ix = self._A[k] == v
        
        subset = self._subset(ix)
        if len(kwargs) == 1:
            return subset
        else:
            kwargs = dict(kwargs[1:])
            return subset.filter(**kwargs)
    
    def regex(self, **kwargs):
        """
        Match one or more label types against regexes.

        Returns
        -------
        A new :class:`GEOSelection` object containing the subset of samples
        matching the regexes.

        Examples
        --------
        >> geo = hta.data.ncbi.GEO("geo.h5")
        >> q = geo.select().regex(Tissue="BTO:12*", Disease="*cancer*")
        """
        kwargs = list(kwargs.items())
        k,v = kwargs[0]
        assert isinstance(v, str)
        
        ix = self._A.index[True == self._A[k].str.match(v)]
        subset = self._subset(ix)
        if len(kwargs) == 1:
            return subset
        else:
            kwargs = dict(kwargs[1:])
            return subset.regex(**kwargs)
    
    def has(self, *args):
        """
        Filter samples according to whether they have a non-null/nan value for
        all of the given label column(s).

        Parameters
        ----------
        args : list
            The label columns to be tested. 

        Returns
        -------
        A new :class:`GEOSelection` object matching the selection.

        Examples
        --------
        >> geo = hta.data.ncbi.GEO("geo.h5")
        >> q = geo.select().has("age", "gender")
        """
        args = list(args)
        k = args.pop()
        ix = self._A.dropna(subset=[k]).index
        subset = self._subset(ix)
        if args:
            return subset.has(*args)
        return subset

    def gt(self, key, value):
        return self._subset(self._A[key] > value)

    def lt(self, key, value):
        return self._subset(self._A[key] < value)

    def gte(self, key, value):
        return self._subset(self._A[key] >= value)

    def lte(self, key, value):
        return self._subset(self._A[key] <= value)

    def eq(self, key, value):
        return self._subset(self._A[key] == value)
   
    ##########
    # Analysis
    ##########

    def contrast(self, key, c1, c2):
        pass
 
    def correlate(self, key, normalize=True, min_size=10):
        return hta.analysis.ma.correlate(self, 
                key, normalize=normalize, min_size=min_size)

    def correlation_report(self, *args, **kwargs):
        return hta.analysis.ma.correlation_report(self, *args, **kwargs)

@hta.util.decorators.application
def _correlate(X, D, key):
    #return X.iloc[:,:500].corrwith(D[key])
    Xf = X.dropna(how="any", axis=1)
    Xn = hta.expression.quantile_normalize(Xf.T).T
    return Xn.corrwith(D[key])

class GEOIterator(object):
    """
    An iterator to platform/series-specific matrices.
    """
    def __init__(self, it, taxon_id, tissue_id):
        self.taxon_id = taxon_id
        self.tissue_id = tissue_id
        self._it = it

    def __iter__(self):
        return self._it

    def apply(self, fn, *args):
        """
        Call fn in parallel with signature fn(X, design, *args).
        """
        # TODO: try pickle if fail do single-CPU
        pool = mp.Pool()
        index = []
        n = []

        def jobs():
            for X in self:
                index.append((X.platform_id, X.series_id))
                n.append(X.shape[0])
                o = [X, X.design]
                o.extend(args)
                yield o
                
        rows = list(pool.map(fn, jobs()))
        index = pd.MultiIndex.from_tuples(index, names=["Platform ID", "Series ID"])
        o = pd.DataFrame(rows, index=index).T
        o.columns.metadata = pd.DataFrame.from_dict({"N": n})
        o.columns.metadata.index = index
        return o


    def _correlation_report(self, key):
        r = self.correlate(key, report=False)
        n_ex = r.apply(lambda x: (~x.isnull()).sum(), axis=1)
        n_ex.name = "N"
        
        cons = (r > 0).sum(axis=1) / n_ex
        cons = pd.Series(np.maximum(cons, 1-cons), index=cons.index)\
                .apply(lambda x: round(x,3))
        cons.name = "Consistency"
        
        r_mu = r.mean(axis=1).apply(lambda x: round(x,3))
        r_mu.name = "Mean_r"
        
        r_median = r.apply(np.median, axis=1).apply(lambda x: round(x,3))
        r_median.name = "Median_r"
        
        # Meta-analytic p-value with Fisher's method
        n = r.columns.metadata["N"]
        t = r * ((n - 2) / (1 - r ** 2)).apply(np.sqrt)
        #sign = np.sign(t)
        #p = 2 * np.minimum(p, 1-p)
        p = stats.t.sf(t, n)
        p = pd.DataFrame(p, index=r.index, columns=r.columns)
        mp = p.apply(hta.statistics.fisher_method, axis=1)
        mp.name = "P-Value"
        
        # Mean rank quantile
        #rank = r.apply(lambda x: pd.Series(np.arange(len(x)), 
        #   index=x.order(ascending=False).index)).mean(axis=1).order()
        #rank /= len(r)
        #rank.name = "Rank Quantile"
        
        # P-value rank
        rank = (pd.Series(np.arange(len(mp)), index=mp.order().index) \
                / len(mp)).apply(lambda x: round(x,3))
        rank.name = "P_Quantile"
        
        #sns.distplot(rp["P-Value"])

        genes = hta.data.ncbi.Entrez.genes(self.taxon_id)
        o = genes\
            .join(n_ex, how="outer")\
            .join(cons, how="outer")\
            .join(r_mu, how="outer")\
            .join(r_median, how="outer")\
            .join(rank, how="outer")\
            .join(mp, how="outer")\
            .dropna(subset=["Symbol"])\
            .sort("P-Value")
            #.dropna()
        #correlations = genes.join(r)
        #correlations.columns = list(correlations.columns)[:2] + \
        #        ["GPL{}/GSE{}".format(*c) for c in r.columns]
        return CorrelationResult(o, r, r.columns.metadata)


    def correlate(self, key, report=False):
        assert isinstance(report, bool)
        if report:
            return self._correlation_report(key)
        else:
            return self.apply(_correlate, key)\
                .dropna(how="all", axis=0)

class GEO(object):
    """
    A HDF5-backed data store containing data from NCBI GEO samples.

    Data is stored by platform, with GSMs as rows and probe IDs or 
    Entrez Gene IDs as columns, at the following HDF5 paths:
        /geo/platform/probe/$gpl
        /geo/platform/gene/$gpl
    """

    # TODO: throw comprehensible error or auto-download when sample/series not found?

    Selection = GEOSelection
    MetaDB = GEOMetaDB

    def __init__(self, path, mode="a"):
        """
        Construct a :class:`GEO` object.

        Parameters
        ----------
        path : str
            File system path to the HDF5 file underlying this database.
        mode : {"r", "a"}
            Open archive in read-only mode ("r") or read-write mode ("a").
        """
        assert mode in ("r", "a")
        LOG.debug("Opening GEO HDF5 database at: '{}'".format(path))

        self._spath = path
        self._smode = mode

        self._store = Store(self._spath, mode=self._smode)
        #self._update_platform_list()

        cache_directory = fs.join(CONFIG["cache"]["root"], "geo")
        self._miniml_cache = fs.join(cache_directory, "miniml")
        os.makedirs(self._miniml_cache, exist_ok=True)

        #label_path = fs.join(cache_directory, "labels.tsv")
        #if not fs.exists(label_path):
        #    with open(label_path, "w") as h:
        #        hta.data.ncbi.geo.annotation.annotate().dump(h, index=False)
        #self._labels = pd.read_csv(label_path, sep="\t")
        LOG.debug("GEO HDF5 database initialized: '{}'".format(path))
       
    def _update_platform_list(self):
        self._platforms = set()
        for k in self._store:
            self._platforms.add(k.split("/")[-1])

    def _sync(self):
        self._store.close()
        self._store = Store(self._spath, mode=self._smode)
        #self._update_platform_list()

    def _path(self, platform_id, level):
        """
        Return HDF path for a GEO platform based at the probe or gene level.
        """
        assert level in ("probe", "gene")
        assert isinstance(platform_id, int)
        platform_id = "GPL{}".format(platform_id)

        if level == "probe":
            hpath = "/geo/platform/probe/{}".format(platform_id)
        elif level == "gene":
            hpath = "/geo/platform/gene/{}".format(platform_id)
        else:
            assert False
        return hpath

    def _frame(self, platform_id, level):
        """
        Retrieve the :class:`h5df.Frame` corresponding to the given platform
        at the probe or gene level.
        """
        hpath = self._path(platform_id, level)
        if not hpath in self._store:
            raise KeyError("Data for platform {} not in store".format(platform_id))
        return self._store[hpath]

    #def select1(self):
    #    ix = self._labels["platform_id"].isin(self._platforms)
    #    return GEO.Selection(self, self._labels.ix[ix,:])

    def query(self, taxon_id, tissue_id, **kwargs):
        it = self._query(taxon_id, tissue_id, **kwargs)
        return GEOIterator(it, taxon_id, tissue_id)
    
    def _query(self, taxon_id, tissue_id, platform_id=None, 
            series_id=None, has=None, gender=None, age_range=None, min_count=5):
        """
        Return a generator of platform/series-specific matrices 
        that match the search parameters.
        """

        q = """
        SELECT 
            s.geo_platform_id AS 'platform_id', 
            m.geo_series_id AS 'series_id', 
            c.geo_sample_id AS 'sample_id', 
            a.gender, a.age
        FROM geo_channel c
        INNER JOIN geo_sample s
            ON c.geo_sample_id=s.id
        INNER JOIN geo_label a
            ON a.channel=c.channel AND a.geo_sample_id=c.geo_sample_id
        INNER JOIN geo_series_sample m
            ON m.geo_sample_id=s.id
        INNER JOIN term_closure tc
            ON tc.descendant_id=a.tissue_id
        INNER JOIN term ta
            ON ta.id=a.tissue_id
        WHERE 
            c.taxon_id={taxon_id}
            AND ta.key='BTO:{tissue_id:07d}';
        """.format(taxon_id=taxon_id, tissue_id=tissue_id)
        design = hta.db.query(q)

        if has is not None:
            design = design.dropna(subset=has)
        if gender is not None:
            assert gender in (0,1)
            design = design.ix[design["gender"] == gender,:]
        if age_range is not None:
            low, high = age_range
            design = design.dropna(subset=["age"])
            design = design.ix[(design["age"] >= low) & (design["age"] <= high),:]
        
        design["sample_id"] = ["GSM{}".format(ix) for ix in design["sample_id"]] 

        for (platform_id, series_id), subset in design.groupby(["platform_id", "series_id"]):
            if subset.shape[0] < min_count:
                continue
            subset = subset.drop(["platform_id", "series_id"], axis=1)\
                    .drop_duplicates(subset=["sample_id"])\
                    .set_index(["sample_id"])
            try:
                X = self.series(platform_id, series_id)
                ix = list(set(X.index) & set(subset.index))
                X = X.loc[ix,:]
                if X.shape[0] < min_count:
                    continue
                X.design = subset
                X.platform_id = platform_id
                X.series_id = series_id
                LOG.debug("Fetched GPL{}/GSE{}".format(platform_id, series_id))
                yield X
            except KeyError:
                LOG.debug("Failed to fetch GPL{}/GSE{} because platform is not imported (had {} annotated samples)".format(platform_id, series_id, subset.shape[0]))
                continue

    def sample(self, sample_id, level="gene"):
        """
        Fetch data for a sample by GEO accession.

        Parameters
        ----------
        sample_id : str
            The GEO sample accession, e.g., "GSMnnn".
        level : {"probe", "gene"}, optional
            Specify whether data is returned at the probe or gene (collapsed) level
            (default "gene").

        Returns
        -------
        :class:`pandas.Series`
            Data vector, with Probe IDs or Entrez Gene IDs as index labels.
        """

        platform_id = hta.db.query("""
            SELECT gpl FROM gsm
            WHERE gsm='{}'""".format(sample_id))[0]
        return self._frame(platform_id, level).row(sample_id)

    def series(self, platform_id, series_id, level="gene"):
        """
        Fetch data for a GEO Series ("GSE") within a given platform ("GPL").

        Parameters
        ----------
        platform_id : str
            The GEO platform accession ("GPLnnn")
        series_id : str
            The GEO series accession ("GSEnnn")
        level : {"probe", "gene"}, optional
            Specify whether data is returned at the probe or gene (collapsed) level
            (default "gene").

        Returns
        -------
        :class:`pandas.DataFrame`
            A data frame with GEO sample IDs ("GSM") as row labels 
            and Probe IDs or Entrez Gene IDs as column labels.

        Notes
        -----
        If only a subset of the GSE is present in the database, it will return this subset.
        If no GSMs are present, will throw KeyError. This is under the assumption that data
        will be imported by platform.
        """
        platform_id = int(platform_id)
        series_id = int(series_id)

        ix = hta.db.query("""
            SELECT s.id
                FROM geo_sample s
            INNER JOIN geo_series_sample m
                ON m.geo_sample_id=s.id
            WHERE m.geo_series_id={} 
                AND s.geo_platform_id={};
        """.format(series_id, platform_id))
        ix = ["GSM{}".format(i) for i in ix]
        frame = self._frame(platform_id, level)
        ix = list(set(frame.index) & set(ix))
        if not ix:
            raise KeyError("No intersection between h5df index and metadata index (probably a bug)")
        return frame.rows(ix)

    def populate(self, min_count=500):
        raise NotImplementedError
        tbl = self._metadb.query("""
            SELECT gpl AS 'platform_id', organism_ch1 AS 'species', COUNT(gpl) AS 'N'
            FROM gsm
            WHERE molecule_ch1='total RNA'
            GROUP BY gpl,organism_ch1
            ORDER BY COUNT(gpl) DESC;""")
        tbl = tbl.ix[tbl["N"] >= min_count,:]
        platforms = list(set(tbl["platform_id"]))
        platforms = list(set(platforms) - set(["GPL5188", "GPL6102"]))
        self.ensure(platforms)

    def ensure(self, platforms, ignore_errors=True):
        """
        For each platform ID in "platforms", check whether it has been loaded
        into the database. If not, download and import it.

        Parameters
        ----------
        platforms : list
            List of platform ID strings (e.g., "GPLnnn") 
        ignore_errors : bool, optional
            If True, partially imported data for the failed GPL will be cleared 
            and import of subsequent datasets will continue. 
            If False, an error will be thrown.
        """

        # TODO: parallel import by asynchronously importing new accessions 
        # into individual HDF5 files, then calling h5df.Store.merge
        # TODO: mark failed GPLs so they won't be retried

        assert isinstance(ignore_errors, bool)

        for platform_id in platforms:
            hpath = self._path(platform_id, "gene")
            if hpath in self._store:
                continue
            try:
                hta.data.stanford.ailun("GPL{}".format(platform_id))
            except:
                LOG.warn("{}: skipping import, no AILUN mapping".format(platform_id))
                continue
            try:
                self._import_matrix(platform_id)
                self._collapse_matrix(platform_id)
            except Exception as e:
                ppath = self._path(platform_id, "probe")
                if ppath in self._store:
                    del self._store[ppath]
                if hpath in self._store:
                    del self._store[hpath]
                fn = LOG.warn if ignore_errors else LOG.error
                fn("{} : error during import: {}".format(platform_id, str(e)))
                if not ignore_errors:
                    raise e

    def _import_matrix(self, accession):
        """
        Fetch, parse, and import probe-level MiniML data from GEO 
        into the HDF5 store.
        """
        CHUNK_SIZE = 100

        assert accession.startswith("GPL")

        hppath = self._path(accession, "probe")
        mpath = fs.join(self._miniml_cache, "{}.tgz".format(accession))

        if not fs.exists(mpath):
            miniml.fetch(accession, mpath)

        # import probe-level data
        LOG.info("{}: Importing probe-level data into HDF".format(accession))
        with tarfile.open(mpath, "r:gz") as h:
            it = miniml.parse(h)
            first = next(it)
            pframe = self._store.create(hppath, first.index, index_type=str)

            it = itertools.chain([first], it)
            for i,chunk in enumerate(hta.util.chunks(it, CHUNK_SIZE)):
                df = pd.concat(chunk, axis=1).T
                pframe.append(df)
                LOG.info("{} probe import : {} processed"\
                        .format(accession, (i+1) * CHUNK_SIZE))

        self._sync()

    def _collapse_matrix(self, accession):
        """
        Collapse probe-level data to gene-level for the given GPL, and
        store the result in the HDF5 store.
        """
        PROBE_SAMPLE_SIZE = 100
        hppath = self._path(accession, "probe")
        hgpath = self._path(accession, "gene")
        pframe = self._store[hppath]

        # probe collapse (by max mean)
        LOG.info("{}: Collapsing probes to genes ...".format(accession))
        pmap = hta.data.stanford.ailun(accession)
        probes, genes = [], []

        LOG.debug("{}: Finding max-mean probe mappings".format(accession))
        rix = random.sample(list(pframe.index), PROBE_SAMPLE_SIZE)
        df_sample = pframe.rows(rix)
        for gene_id, subset in df_sample.groupby(pmap, axis=1):
            mu = subset.mean()
            mu.sort()
            probes.append(mu.index[-1])
            genes.append(int(gene_id))

        LOG.debug("{}: Storing collapsed data".format(accession))
        gframe = self._store.create(hgpath, genes, index_type=str)
        for ixchunk in hta.util.chunks(pframe.index, 100):
            pck = pframe.rows(ixchunk)
            gck = pck.loc[:,probes]
            gck.columns = genes
            gframe.append(gck)

        self._sync()
