import tempfile
import sqlite3
import subprocess as sp
import os

from collections import namedtuple, defaultdict

import pandas as pd

import hta.util
from hta.util import fs, CONFIG, memoize, LOG
from hta.data.ncbi import Entrez
import hta.ontology

SCHEMA = """
CREATE TABLE platform (
    id INTEGER PRIMARY KEY NOT NULL,
    title VARCHAR
);

CREATE TABLE series (
    id INTEGER PRIMARY KEY NOT NULL,
    title VARCHAR,
    summary VARCHAR,
    design VARCHAR
);

CREATE TABLE sample (
    id INTEGER PRIMARY KEY NOT NULL,
    platform_id INTEGER NOT NULL,
    title VARCHAR,
    description VARCHAR,

    FOREIGN KEY (platform_id) REFERENCES platform (id)
);

CREATE TABLE channel (
    sample_id INTEGER NOT NULL,
    channel INTEGER NOT NULL,
    source_name VARCHAR,
    molecule VARCHAR,
    characteristics VARCHAR,
    taxon_id INTEGER,

    PRIMARY KEY (sample_id, channel),
    FOREIGN KEY (sample_id) REFERENCES sample (id)
);

CREATE TABLE series_sample (
    series_id INTEGER NOT NULL,
    sample_id INTEGER NOT NULL,

    FOREIGN KEY (series_id) REFERENCES series (id),
    FOREIGN KEY (sample_id) REFERENCES sample (id),
    UNIQUE (series_id, sample_id)
);

CREATE TABLE label (
    sample_id INTEGER NOT NULL,
    channel INTEGER NOT NULL,

    gender INTEGER,
    age FLOAT,
    tissue INTEGER,

    PRIMARY KEY (sample_id, channel),
    FOREIGN KEY (sample_id) REFERENCES sample (id),
    FOREIGN KEY (sample_id, channel) REFERENCES channel (sample_id, channel)
);

CREATE TABLE tissue_closure (
    ancestor INTEGER NOT NULL,
    descendant INTEGER NOT NULL,

    PRIMARY KEY (ancestor, descendant)
);

"""

INDEXES = """
CREATE INDEX ix_series_sample_series_id ON series_sample (series_id);
CREATE INDEX ix_series_sample_sample_id ON series_sample (sample_id);
CREATE INDEX ix_label_sample_id_channel ON label (sample_id, channel);
CREATE INDEX ix_channel_sample_id ON channel (sample_id);
CREATE INDEX ix_sample_platform_id ON sample (platform_id);
CREATE INDEX ix_tissue_closure_ancestor ON tissue_closure (ancestor);
CREATE INDEX ix_tissue_closure_descendant ON tissue_closure (descendant);
"""

def _connect():
    # FIXME: probably should use python gzip
    opath = fs.join(CONFIG["cache"]["root"], "geo", "GEOmetadb.sqlite")
    if not fs.exists(opath):
        os.makedirs(fs.dirname(opath), exist_ok=True)
        URL = "http://gbnci.abcc.ncifcrf.gov/geo/GEOmetadb.sqlite.gz"
        ipath = hta.util.download(URL)
        exe = fs.which("pigz") or "gzip"
        with open(opath, "wb") as h:
            sp.check_call([exe, "-cd", ipath], stdout=h)
    return sqlite3.connect(opath)

@memoize
def get_taxon_map():
    taxonomy = Entrez.taxonomy()
    return dict(zip(taxonomy["name"], map(int, taxonomy.index)))

def initialize(path):
    LOG.info("Initializing GEO metadata DB")
    mdb = _connect()
    mc = mdb.cursor()
    db = sqlite3.connect(path)
    c = db.cursor()
    c.executescript(SCHEMA)

    mc.execute("SELECT CAST(SUBSTR(gpl,4) AS integer), title FROM gpl;")
    c.executemany("INSERT INTO geo_platform VALUES (?,?);", iter(mc))

    mc.execute("""SELECT CAST(SUBSTR(gse,4) AS integer), title, summary, overall_design 
        FROM gse;""")
    c.executemany("INSERT INTO geo_series VALUES (?,?,?,?);", iter(mc))

    mc.execute("""SELECT CAST(SUBSTR(gsm,4) AS integer), CAST(SUBSTR(gpl,4) AS integer), 
        title, description FROM gsm;""")
    c.executemany("INSERT INTO geo_sample VALUES (?,?,?,?);", iter(mc))

    taxon_map = get_taxon_map()
    def replace_organism(it, i):
        for row in map(list, it):
            taxon_id = taxon_map.get(row[i])
            if taxon_id is not None:
                row[i] = taxon_id
                yield row

    mc.execute("""SELECT CAST(SUBSTR(gsm,4) AS integer), 1, 
            source_name_ch1, molecule_ch1, characteristics_ch1, organism_ch1
            FROM gsm;""")
    c.executemany("INSERT INTO geo_channel VALUES (?,?,?,?,?,?);", 
            replace_organism(iter(mc), 5))

    mc.execute("""SELECT CAST(SUBSTR(gsm,4) AS integer), 2, 
                source_name_ch2, molecule_ch2, characteristics_ch2, organism_ch2
            FROM gsm WHERE channel_count >= 2;""")
    c.executemany("INSERT INTO geo_channel VALUES (?,?,?,?,?,?);", 
            replace_organism(iter(mc), 5))

    mc.execute("""SELECT CAST(SUBSTR(gse,4) AS integer), CAST(SUBSTR(gsm,4) AS integer) 
        FROM gse_gsm;""")
    c.executemany("INSERT INTO geo_series_sample VALUES (?,?);", iter(mc))

    #o = hta.ontology.fetch("BTO")
    #c.executemany("INSERT INTO tissue_closure (ancestor, descendant)
    #    VALUES (?,?)", ((int(a),int(d)) for a,d 
    #        in o.ancestry_table.to_records(index=False)))

    db.commit()

    LOG.info("GEO metadata DB : indexing")
    c = db.cursor()
    c.executescript(INDEXES)
    db.commit()

    LOG.info("GEO metadata DB initialized")

    tables = ["platform", "series", "sample", "channel", "series_sample"]
    for table in tables:
        c.execute("SELECT COUNT(*) FROM {};".format(table))
        LOG.debug("{} : {}".format(table, list(c)[0][0]))
    db.close()

def update_labels(path):
    db = sqlite3.connect(path)
    c = db.cursor()
    c.execute("DELETE FROM label;")
    db.commit()

    c = db.cursor()
    def records():
        for row in hta.data.ncbi.geo.annotation.annotate():
            if any(row[2:]):
                yield row
    c.executemany("""INSERT INTO label (sample_id, channel, tissue_id, gender, age) 
            VALUES (?,?,?,?,?);""", records())
    db.commit()
    db.close()

Sample = namedtuple("Sample",
        ["accession", "channel", "experiments", "taxon_id",
            "platform", "title", "description", "characteristics", "source"])
Sample.text = property(lambda s: "\n".join(map(lambda x: x or "", 
    [s.title, s.description, s.characteristics, s.source])))

Experiment = namedtuple("Experiment",
        ["accession", "title", "summary", "design"])
Experiment.text = property(lambda s: "\n".join(map(lambda x: x or "", 
    [s.title, s.summary, s.design])))

class GEOMetaDB(object):
    def __init__(self):
        path = fs.join(CONFIG["cache"]["root"], "geo", "meta.db")
        os.makedirs(fs.dirname(path), exist_ok=True)
        if not fs.exists(path):
            initialize(path)
            update_labels(path)
        self._cx = sqlite3.connect("file://{}?mode=ro".format(path),
                uri=True)

    def query(self, sql):
        o = pd.read_sql(sql, self._cx)
        if o.shape[1] == 1:
            return list(o.iloc[:,0])
        else:
            return o

    @property
    def samples(self):
        m = defaultdict(set)
        for gsm,gse in self.query("SELECT series_id,sample_id FROM series_sample")\
                .to_records(index=False):
            m[gsm].add(gse)

        it = self.query("""
            SELECT s.id, c.channel, s.platform_id, s.title, s.description,
                c.characteristics,c.taxon_id,c.source_name
            FROM sample s
            INNER JOIN channel c
                ON c.sample_id=s.id
            WHERE 
            c.molecule='total RNA';""").to_records(index=False)
        for sample_id,i,platform_id,title,desc,ch,taxon_id,source in it:
            yield Sample(int(sample_id),int(i),m[gsm],int(taxon_id),
                    platform_id,title,desc,ch,source)

    @property
    def experiments(self):
        for row in self.query("SELECT * FROM series;").to_records(index=False):
            row = list(row)
            row[0] = int(row[0])
            yield Experiment(*row)

if __name__ == "__main__":
    db = GEOMetaDB()
