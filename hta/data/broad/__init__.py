import os.path

from hta.data.ncbi import Entrez
from hta.data.annotation import AnnotationSet

def msigdb(taxon_id=9606):
    all_genes = set(Entrez.genes(taxon_id).index)
    path = os.path.join(os.path.dirname(__file__), 
            "msigdb.v5.0.entrez.gmt")
    names = {}
    pairs = []

    with open(path, "r") as h:
        for line in h:
            fields = line.split("\t")
            term_id = name = fields[0]
            names[term_id] = name
            genes = set(list(map(int, fields[2:]))) & all_genes
            for gene_id in genes:
                pairs.append((gene_id, term_id))

    return AnnotationSet(names, pairs)
