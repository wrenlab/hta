import pandas as pd

from hta.data.ncbi import Entrez
from hta.data.annotation import AnnotationSet
from hta.util import download, memoize

@memoize
def miRDB(taxon_id):
    family = {
            9615: "cfa",  # dog
            9031: "gga",  # chicken
            9606: "hsa",  # human
            10090: "mmu", # mouse
            10116: "rno"  #rat
    }
    assert taxon_id in family
    prefix = family[taxon_id]

    # Native format: miR name, target Genbank, score
    path = download("http://mirdb.org/miRDB/download/miRDB_v5.0_prediction_result.txt.gz")
    tbl = pd.read_csv(path, sep="\t", compression="gzip", header=None)
    tbl.columns = ["miR", "Target", "Score"]
    tbl = tbl.ix[tbl["miR"].str.startswith(prefix),:]

    # map target to Entrez ID
    q = """
        SELECT ga.nucleotide_accession, ga.gene_id 
        FROM gene_accession ga 
        INNER JOIN gene 
            ON gene.id=ga.gene_id 
        WHERE gene.taxon_id={}
            AND ga.nucleotide_accession IS NOT NULL;""".format(taxon_id)
    m_target = dict(Entrez.query(q).to_records(index=False))
    tbl["Target"] = [m_target.get(tgt) for tgt in tbl["Target"]]

    # map miR to Entrez ID
    genes = Entrez.genes(taxon_id)
    m_mir = {}
    for id,s in zip(genes.index, genes["Symbol"]):
        if not s.lower().startswith("mir"):
            continue
        s = s[3:]
        if s.startswith("let"):
            s = "let-" + s[3:]
        m_mir[s] = id

    mir_to_id = {}
    for k in tbl["miR"]:
        if k.startswith(prefix):
            key = "-".join(k.split("-")[1:-1])
            id = m_mir.get(key)
            if id is None:
                id = m_mir.get("-".join(key.split("-")[1:]))
            if id is not None:
                mir_to_id[k] = id
    tbl["miR"] = [mir_to_id.get(mir) for mir in tbl["miR"]]

    tbl = tbl.dropna()
    tbl["miR"] = tbl["miR"].astype(int)
    tbl["Target"] = tbl["Target"].astype(int)
    tbl = tbl.drop_duplicates(subset=["miR", "Target"])

    metadata = Entrez.genes(10090)
    metadata = metadata.ix[metadata.index.isin(set(tbl["Target"])),:]
    return AnnotationSet(tbl, metadata=metadata)
