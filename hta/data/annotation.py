"""
Gene annotations -- mapping of genes to pathways or functions.
"""

import collections

import pandas as pd
import numpy as np
from scipy import stats

import hta.ontology
import hta.util

class AnnotationSet(object):
    """
    A bidirectional map of Entrez Gene IDs to Term IDs, with an additional
    "background" attribute listing all "elements" (usually genes)
    annotated by any term in the set.
    """

    def __init__(self, mapping, metadata=None):
        """
        Arguments
        ---------
        mapping: a :class:`pandas.DataFrame` with 2 or 3 columns: 
            Element IDs, Term IDs, and (optional) a score associated with the relationship
        metadata: :class:`pandas.DataFrame`, optional
            Term IDs are row indices, columns contain metadata about each term (e.g., names)
        """

        assert isinstance(mapping, pd.DataFrame)
        assert(mapping.shape[1] in (2,3))
        if mapping.shape[1] == 2:
            mapping["Score"] = 1
        mapping.columns = ["Element ID", "Term ID", "Score"]
        self.mapping = mapping.drop_duplicates(subset=["Element ID", "Term ID"]) 
        self.metadata = metadata

    def __repr__(self):
        return "<AnnotationSet with {} terms and {} elements>".format(len(self.elements),
                len(self.terms))

    @property
    def elements(self):
        return list(sorted(set(self.mapping["Element ID"])))

    @property
    def terms(self):
        return list(sorted(set(self.mapping["Term ID"])))

    def enrichment(self, scores, min_count=3):
        """
        Perform enrichment analysis for the association of a scored set of genes 
        with terms using Fisher's Exact test.

        Arguments
        ---------
        scores: a :class:`pandas.Series` with Entrez Gene IDs as indices and
            numeric or boolean values  
        min_count: int
            The minimum number of selected genes which must be annotated with the 
            term in order to consider the term for enrichment.

        Returns
        -------
        A :class:`pandas.DataFrame` with statistics showing enrichment values 
        for each term.
        """
        return self._enrichment_binary(scores, min_count)

    def _enrichment_permutation(self, scores):
        raise NotImplementedError

    def _enrichment_binary(self, scores, min_count):
        background = list(set(self.elements) & set(scores.index))
        scores = scores.loc[background]

        if (scores.dtype == np.bool):
            s = scores
        else:
            s = scores > scores.quantile(0.95)
        s = s.astype(int)
        s.name = "Score2"
        m = self.mapping.ix[self.mapping["Element ID"].isin(background),:].copy()
        m["Score"] = 1
        j = m.merge(s.to_frame(), how="inner", left_on="Element ID", right_index=True)\
                .dropna(subset=["Term ID"])
        counts = j.groupby(["Score2", "Term ID"])["Score"].count()

        # as a s
        ct = pd.concat([
                counts.loc[1],
                s.sum() - counts.loc[1],
                counts.loc[0]], axis=1)
        ct.columns = ["AS", "S", "A"]
        ct = ct.fillna(0)
        ct["O"] = len(s) - ct.sum(axis=1)
        ct = ct.ix[ct["AS"] >= min_count,:]
        assert ((ct["AS"] + ct["S"]) == s.sum()).all()
        assert ((ct["A"] + ct["O"]) == (len(s) - s.sum())).all()

        o = []
        for term_id in ct.index:
            cts = ct.loc[term_id,:]
            cts = np.array(cts).reshape((2,2))
            OR, p = stats.fisher_exact(cts)
            slpv = -1 * np.sign(np.log(OR)) * np.log10(p)
            o.append((term_id, OR, slpv))
        o = pd.DataFrame(o, columns=["Term ID", "Odds Ratio", "SLPV"])\
                .set_index(["Term ID"])
        o = ct.join(o, how="inner")
        if self.metadata is not None:
            o = self.metadata.join(o, how="right")
        return o.sort("SLPV", ascending=False)

@hta.util.memoize
def gene_ontology(taxon_id):
    o = hta.ontology.fetch("GO")
    path = hta.util.download("ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2go.gz")
    X = pd.read_csv(path, 
            compression="gzip", sep="\t", skiprows=[0], 
            usecols=(0,1,2,3), header=None)
    X.columns = ["Taxon ID", "Gene ID", "Term ID", "Evidence"]
    ix = (X["Taxon ID"] == taxon_id) & (X["Evidence"] != "IEA")
    mapping = X.ix[ix,["Gene ID", "Term ID"]]
    return AnnotationSet(mapping, metadata=o.terms.drop("Namespace", axis=1))
