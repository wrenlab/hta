"""
API for meta-analysis of high-throughput biological datasets.

General design
==============

A meta-analysis requires the following types of data:

1. A *measurement matrix* of samples (rows) versus entities (columns)
    For example, a gene expression matrix, which may consist of samples 
    from multiple experiments and platforms, and a consistent set of genes.

2. *sample attributes* (or "labels"): These generally consist of two types.
    a.  Information about the technology or context of the sample 
        (microarray platform, experiment ID, etc.). The assumption is that
        samples are not directly comparable unless they share these attributes.
        In other words, groups of samples from the same platform and experiment
        will be analyzed separately and the results combined using meta-analysis 
        methods.
    b.  Biological attributes of an individual sample, including age, gender,
        tissue of origin, as well as experimental manipulations such as diet,
        genetic alteration, etc.

3. *entity attributes*: Most commonly, an "entity" will be a gene. 
    Can be a gene's name, symbol, etc.

Note that a sample or entity can have a one-to-one or one-to-many relationship
with a particular attribute. A sample generally only has one age or technology platform,
but can actually be part of multiple experiments. Likewise, a entity/gene has one
canonical symbol and name, but often multiple associated GO terms.

Limitations
===========

Ideally, the algorithms used for meta-analysis could also be applied to individual
datasets.
"""

from .expression import *
