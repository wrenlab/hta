import numpy as np

Region = np.dtype([
    ("contig", np.str_, 32),
    ("start", np.uint64),
    ("end", np.uint64),
    ("name", np.str_, 256),
    ("score", np.float64),
    ("strand", np.str_, 1)])

def region(contig, start, end, name=None, score=None, strand="."):
    if name is None:
        name = ""
    return np.array([(contig, start, end, name, score, strand)],
            dtype=Region)[0]
