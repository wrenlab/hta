import numpy as np
from scipy import stats

def fisher_method(pvs):
    x2 = -2 * pvs.apply(np.log).sum()
    df = 2 * len(pvs) 
    return stats.chi2.sf(x2, df)
