from os.path import exists as pexists, expanduser as pexpand, join as pjoin
from os.path import basename, splitext, dirname

import base64
import functools
import gzip
import itertools
import logging
import os
import os.path
import pickle
import sqlite3
import sys
import tempfile
import urllib.request
import urllib.request
import zlib

import joblib
import pandas as pd
import numpy as np
import yaml

#########
# Logging
#########

logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger("hta")
LOG.setLevel(logging.DEBUG)

###############
# Configuration
###############

def _fix_configuration(e):
    if isinstance(e, dict):
        for k,v in e.items():
            e[k] = _fix_configuration(v)
        return e
    elif isinstance(e, str):
        return e.replace("$HOME", pexpand("~"))
    else:
        return e
            
def load_configuration():
    CONFIG_SEARCH = [pjoin(pexpand(folder), "hta.yml") for folder in [
            ".",
            "~/.config/hta/"
    ]] + [pjoin(dirname(__file__), "config.default.yml")]

    for path in CONFIG_SEARCH:
        if os.path.exists(path):
            with open(path) as h:
                config = _fix_configuration(yaml.load(h))
                LOG.debug("Configuration loaded from: {}".format(path))
            break
    return config

CONFIG = load_configuration()

###########
# Functions
###########

def as_float(x):
    try:
        return float(x)
    except ValueError:
        return np.nan

def chunks(it, size=1000):
    """
    Divide an iterator into chunks of specified size. A
    chunk size of 0 means no maximum chunk size, and will
    therefore return a single list.
    
    Returns a generator of lists.
    """
    if size == 0:
        yield list(it)
    else:
        chunk = []
        for elem in it:
            chunk.append(elem)
            if len(chunk) == size:
                yield chunk
                chunk = []
        if chunk:
            yield chunk

def download(url):
    """
    Download a file over HTTP or FTP, cache it locally, and return a path to it.
    """
    # TODO: add "expire" parameter
    cache = os.path.join(CONFIG["cache"]["root"], "download")
    os.makedirs(cache, exist_ok=True)
    target = base64.b64encode(url.lower().encode("utf-8"))\
            .decode("utf-8")
    path = os.path.join(cache, target)
    if not os.path.exists(path):
        LOG.info("Cache miss: {}".format(url))
        urllib.request.urlretrieve(url, path)
    else:
        LOG.info("Cache hit: {}".format(url))
    return path

class CachedDB(object):
    """
    API for a remote SQLite database, locally caching the file and with a
    simplified pandas-based query API.

    Inheritors must provide the "URL" attribute, and call this __init__ from
    their ctor.
    """
    CACHE_DIRECTORY = pjoin(CONFIG["cache"]["root"], "db")
    URL = None

    def __init__(self):
        self.reconnect()

    def _download(self, url, path):
        os.makedirs(os.path.dirname(path), exist_ok=True)

        block_size = 4096
        dc = zlib.decompressobj(16+zlib.MAX_WBITS)
        cx = urllib.request.urlopen(self.URL)
        with open(path, "wb") as o:
            while True:
                data = cx.read(block_size)
                if not data:
                    break
                o.write(dc.decompress(data))
        cx.close()

    def reconnect(self):
        name = basename(self.URL)
        if name.endswith(".gz"):
            name = splitext(name)[0]
        path = pexpand(pjoin(self.CACHE_DIRECTORY, name))

        if not pexists(path):
            self._download(self.URL, path)

        self._cx = sqlite3.connect("file://%s?mode=ro" % path,
                uri=True)

    def query(self, sql):
        """
        Query the GEOMetaDB using the given SQL. 

        Parameters
        ----------
        sql : str
            The SELECT sql.

        Returns
        -------
        If the SQL returns one column, returns a :class:`list`. If more than one
        column, returns a :class:`pandas.DataFrame`.

        Notes
        -----
        SQL that attempts to alter the database (INSERT, UPDATE) will fail,
        as the underlying database handle is opened read-only.
        """
        self.reconnect()
        o = pd.read_sql(sql, self._cx)
        if o.shape[1] == 1:
            return list(o.iloc[:,0])
        else:
            return o

# Memoization

memory = joblib.Memory(cachedir=pjoin(CONFIG["cache"]["root"], "memoize"), verbose=1)
memoize = memory.cache

def static_memoize(path, compress_level=5):
    """
    A decorator that wraps a no-arg function and memoizes its result to the
    supplied file path.

    Arguments
    ---------
    path: str
        Path to serialize the output to.
    compress_level: int, optional, 0-9
        If in the range (1-9), this compression level will be used by gzip.
        If compress_level == 0, then output will be stored decompressed.

    Examples
    --------

    @static_memoize("foo.memo")
    def fn():
        return 5
    """
    assert isinstance(compress_level, int)
    assert compress_level >= 0
    assert compress_level <= 9

    use_gzip = compress_level > 0
    open_fn = lambda path, mode: gzip.open(path, mode, compresslevel=compress_level) if use_gzip else open

    def wrapper(fn):
        def wrap():
            if not os.path.exists(path):
                rs = fn()
                with open_fn(path, "wb") as h:
                    pickle.dump(rs, h)
            with open_fn(path, "rb") as h:
                return pickle.load(h)
        return wrap
    return wrapper

# pandas

class StreamingDataFrame(object):
    """
    An object wrapping an iterable of :class:`pandas.Series` that can be
    lazily processed or output, or coerced to a full 
    :class:`pandas.DataFrame`.
    """
    def __init__(self, iterator):
        first = next(iterator)
        assert isinstance(first, pd.Series)
        self._columns = list(first.index)
        self._it = itertools.chain([first], iterator)

    def __iter__(self):
        """
        Return the underlying iterator of :class:`pandas.Series`. Note, if elements 
        are consumed by the caller, they will also be consumed from this object. 
        """
        return self._it

    def dump(self, handle=sys.stdout, sep="\t", header=True, index=True):
        """
        Lazily print rows to the provided handle.

        Arguments
        ---------
        handle : file
            A file-like object to print output to.
        sep : str
            Field delimiter.
        header : bool
            Whether to print column names.
        index : bool
            Whether to print row names.
        """
        if header:
            columns = []
            if index:
                columns.append("")
            columns.extend(self._columns)
            print(*columns, sep="\t", file=handle)

        for row in self._it:
            if index:
                print(row.name, end=sep, file=handle)
            print(*row, sep=sep, file=handle)

    def head(self, n=10):
        assert n >= 1
        rows = list(itertools.islice(self._it, n))
        self._it = itertools.chain(rows, self._it)
        return pd.DataFrame(rows) 

    def to_frame(self):
        return pd.DataFrame(list(self._it))

def dfstream(fn):
    @functools.wraps(fn)
    def wrap(*args, **kwargs):
        it = fn(*args, **kwargs)
        return StreamingDataFrame(it)
    return wrap
