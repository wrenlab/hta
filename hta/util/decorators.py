import functools

def application(fn):
    """
    Wrap a function that takes multiple arguments that will
    be passed as a tuple. Useful for functions to be used via 
    multiprocessing.
    """

    @functools.wraps(fn)
    def wrap(args):
        return fn(*args)
    return wrap
