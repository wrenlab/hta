__all__ = [
    "rm", "mv", "which", "ls",

    # stdlib exports
    "exists", "dirname", "join"
]

import os
import os.path
from os.path import exists, dirname, join
from os.path import expanduser as expand
import shutil
import sys

def rm(path, recursive=False, force=True):
    try:
        if os.path.isdir(path):
            if not recursive:
                raise Exception("Path is directory and recursive is False: {}".format(path))
            else:
                shutil.rmtree(path)
        else:
            os.unlink(path)
    except FileNotFoundError as e:
        if not force:
            raise e

def mv(src, dest):
    shutil.move(src, dest)

def ls(dir):
    dir = os.path.abspath(dir)
    o = []
    for fname in os.listdir(dir):
        o.append(join(dir, fname))
    return o

def which(binary_name, additional_paths=None):
    """
    Search PATH for binary "binary_name". Returns None if it could not be found.
    """
    search_folders = list(sys.path)
    search_folders.extend([
        "/usr/bin/",
        "/usr/local/bin/",
        "/sbin/"])
    if additional_paths is not None:
        search_folders.extend(additional_paths)
    for folder in search_folders:
        path = os.path.join(folder, binary_name)
        if os.path.isfile(path) and os.access(path, os.X_OK):
            return path


