"""
Python wrapper for the Aspera client (used for fast downloads from NCBI
server).
"""

# FIXME: if the user hasn't manually used ascp before, download()
# will hang, asking the user to trust the remote host and add
# remote key to PuTTY cache

# TODO: download binary if necessary?
# TODO: allow option to AsperaClient.download() to background the download
#   into other process or thread
# TODO: if binary is not found, add instructions into error message
#   about how to get it
# TODO: allow multiple source files to be given to download() ?

import os
import sys
import subprocess as sp
import tempfile

from hta.util import LOG, fs

class Client(object):
    # NOTE: Newer versions of ascp use asperaweb_id_dsa.openssh, 
    #   whereas older ones use aspweraweb_id_dsa.putty
    # The right way to handle this would be to figure out when it changed
    # and detect ascp version.
    def __init__(self, user, host,
            limit=200, # in MB
            key_file="~/.aspera/connect/etc/asperaweb_id_dsa.openssh"):
        assert isinstance(limit, int)

        self._binary_path = fs.which("ascp", 
                [os.path.expanduser("~/.aspera/connect/bin/")])
        if not self._binary_path:
            raise Error("Aspera client binary (ascp) could not be found.")

        self._user = user
        self._host = host
        self._limit = limit
        self._key_file = os.path.expanduser(key_file)

    def download(self, src, dest, verbose=False):
        """
        Download the file or directory from the server to the destination path.
        """
        # Possible enhancements: 
        # - download in background process 
        # - allow callback when download completed
        dest = os.path.abspath(dest)
        source_uri = "%s@%s:/%s" % (self._user, self._host, src.lstrip("/"))
        args = [self._binary_path, 
                "-i", self._key_file, 
                "-T",
                "-l", "%sM" % self._limit,
                source_uri, dest]

        LOG.debug("Starting Aspera download: {}".format(source_uri))
        stdout = sys.stdout
        stderr = sys.stderr
        if not verbose:
            stdout = stderr = sp.DEVNULL

        sp.check_call(args, stdout=stdout, stderr=stderr)
        LOG.debug("Aspera download complete: {}".format(source_uri))
