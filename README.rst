=================================================================================
hta - Utilities for analysis and meta-analysis of high-throughput biological data
=================================================================================

This package provides tools to analyze high-throughput biological datasets
(primarily microarray or HTS) either as individual experiments, or in the
context of meta-analysis. Some of the tools provided are:

- Methods to easily fetch, process, store, and analyze large datasets from NCBI
  GEO in HDF5
- Implementations of, or API interfaces to, common gene expression analysis tools,
  including normalization, DE, and enrichment analysis
- Statistical methods to perform meta-analysis on GEO and user-provided datasets

Dependencies
============

Binary/CLI dependencies:

- bedtools
- bedGraphToBigWig (Kent utilities)
- samtools
- bedops
