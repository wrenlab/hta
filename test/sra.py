import tempfile
import os

from hta.data.ncbi.sra import SRA, get_assembly_list

def test_sra(root="test_data/sra"):
    assembly_key = "WBcel235"
    runs = ["SRR2142254", "SRR2142255"]

    db = SRA(root)
    db.ensure_runs(runs)
    assembly = db.assembly(assembly_key)

    for run_id in runs:
        assembly.align(run_id)
    print(assembly.expression())

if __name__ == "__main__":
    root = "test_data/sra"
    test_sra(root=root)

    #with tempfile.TemporaryDirectory() as wd:
    #    test_sra(root=wd.name)
