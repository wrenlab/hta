Method for collaping affy probes:
http://www.cbs.dtu.dk/biotools/jetset/

Drug signatures for gene set analysis (DSigDB)
http://bioinformatics.oxfordjournals.org/content/31/18/3069.full

scope/layers
============

1. Core analysis
-----------------

purpose: "DE"/correlation style) of individual HT dataset

- input: emat and labels (maybe, depending on type)

- basically, fn_core(emat, design, contrast(?))

breakdown:
- *correlate(X, something)*, where something could be a numeric 
  attribute of a sample or gene, or a column or row vector of the emat
- *model(X, design, contrast)*: DEA

2. Meta-analysis
----------------

purpose: doing (1) on multiple datasets and aggregating the results

- input: labels and multiple emats or a selection on a big one (IOW, a way to
  define what is one "experiment" to be analyzed, or the "unit" of MA)

- basically, fn_combine(map(fn_core, datasets)) or possibly reduce(fn_combine, map(fn_core, datasets))

- possible parallelism

- does this really do anything?? I guess the tricky part is "fn_combine"....
  not sure if it can be done as a reduction either, it may have to realize the 
  whole collection

breakdown:
- as above, where a "dataset" is emat+design+contrast...maybe. note that
  a core analysis can require only emat or emat + "design" (one vector) 
  for *correlate*.

3. downstream analysis
----------------------

purpose: taking "DEG" vector of numbers and doing something with it

- associating results with ontology terms or gene sets

- text mining (?; probably TM-derived gene sets will be treated like regular
  gene sets and any actual text mining will be done in an entirely different
  package)

breakdown:
- fet
- multifet (for a collection of onto terms, has to wrap fet B/C background is shared)
- gsea
- etc.

4. high-level API 
-----------------

purpose: user-friendly layer over (1/2/3). the bulk of work should be done by (2) and (3) APIs.

input: 
- basically anything parametric: what labels/ontologies/entity type to use, and what query to run

work:
- retrieval of data from various databases
- calling (1) and (2) functions to get results
- associate results with user-readable stuff (gene names, ontology term names) 

output:
- tables or figures

breakdown:
- no clue....this will be hardest by far

notes

- maybe including data storage for big emats, etc?????

- perhaps 2/3 could be designed so they are passed data structures and the actual
  storage backend will be deferred as long as possible

- this will be hard because 1-2 are basically nonparametric, 3 could be
  nonparametric if done carefully, but the user here has to specify what
  ontologies/expr data sources/etc to use.

- outputting related attributes of genes like names and symbols

use scenarios
-------------

individual dataset analysis: (4) -> (1) -> (3)
meta-analysis: (4) -> ((2) -> (1)) -> (3)

parts
=====

major parts:
- "background data", relatively static, like gene info, gene-ontology mappings from 
  NCBI & friends
- "measurement data" (aka expression), for meta-analyses would be stored in
  a (probably HDF) database but could be provided directly too for small-scale analysis
- "sample data" (labels, experiment, platform), there are different types and they would
  differ depending on the data source

entities:
- "database" (?)
- sample
- gene
- ontology / gene set
- experiment

1-1 relations (aka attributes):
- sample metadata text, platform (?)
- sample labels
- gene name, symbol

1-many relations:
- sample to experiment

many-many:
- gene to ontology term or GS
- term-term relations

thoughts
========

should ontology analysis even be included directly? perhaps should be pared
down to "give me a list of genes and numbers" types of analysis and downstream
separate

an ontology term is a gene set + relations to other terms

formalize all this in SQL? (initial thought: formalize only what is unlikely to
change and not GEO-specific)

hardcode the idea of a "gene" as the thing to be measured, or leave room for
protein/transcript/exon/whatever? 
