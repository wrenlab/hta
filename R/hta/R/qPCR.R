#library(HTqPCR)
#library(nondetects)

impute.qPCR = function(expression, design, controls, max.cycles=NULL) {
    X = as.matrix(expression)
    if (is.null(max.cycles)) {
        max.cycles = max(X, na.rm=T)
    }
    controls = intersect(rownames(X)[complete.cases(X)], controls)

    stopifnot(
              colnames(expression) == rownames(design),
              controls %in% rownames(expression),
              length(controls) >= 1
    )

    cat = array("OK", c(nrow(X),ncol(X)))
    dimnames(cat) = dimnames(X)
    cat[is.na(X)] = "Undetermined"
    cat = as.data.frame(cat)

    complete = complete.cases(X)

    X[is.na(X)] = max.cycles

    obj = new("qPCRset", 
              exprs=X,
              featureCategory=cat)
    sampleNames(obj) = colnames(X)
    featureNames(obj) = rownames(X)

    groups = colnames(design)
    design$sampleName = colnames(X)
    pData(obj) = design

    ftype = rep("target", nrow(X))
    names(ftype) = rownames(X)
    ftype[controls] = "control"
    featureType(obj) = ftype

    rs = qpcrImpute(obj, groupVars=groups)
    exprs(rs)
}
