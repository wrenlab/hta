from os.path import dirname, join as pjoin, exists as pexists
from pip.req import parse_requirements
from pip.download import PipSession
from setuptools import setup, find_packages

requirements = pjoin(dirname(__file__), "requirements.txt")
assert pexists(requirements)
requirements = [str(r.req) for r in parse_requirements(requirements, session=PipSession())]

setup(
    name="hta",
    version="0.1",
    description="Utilities for analysis and meta-analysis of high-throughput biological data",
    author=", ".join([
        "Cory Giles", 
        "Jonathan Wren", 
        "Mikhail Dozmorov", 
        "Aleksandra Perz",
        "Chris Reighard", 
        "Xiavan Roopnarinesingh"
        ]),
    author_email="mail@corygil.es",
    include_package_data=True,
    packages=find_packages(),
    url="http://bitbucket.org/wrenlab/hta",
    install_requires=requirements
)
